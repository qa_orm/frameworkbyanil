package com.OneReverse;
import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.FPath;
import com.UtilityFiles.ReportStatus;
public class Client {
	static com.UtilityFiles.CommonFunctions data;
	public static final String FILEPATH_TESTDATA = FPath.TESTDATA;
	public static final String ID = "com.OneReverse.Client";

	ReportStatus rs=new ReportStatus();
	//public ReportStatus Loan(Integer rowCount) throws Exception {
	
	public  ReportStatus Client(Integer rowCount) throws Exception {
		
		rs.setReportStatus(false);
		
		
		
		OneRev.Client(CommonFunctions.getTestData(FILEPATH_TESTDATA, "FirstName", ID,rowCount),
	CommonFunctions.getTestData(FILEPATH_TESTDATA, "MiddleName", ID,rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "LastName", ID,rowCount),
			CommonFunctions.getTestData(FILEPATH_TESTDATA, "CurrentAddress", ID,rowCount),
				CommonFunctions.getTestData(FILEPATH_TESTDATA, "CurrentCity", ID,rowCount),
				CommonFunctions.getTestData(FILEPATH_TESTDATA, "CurrentState", ID,rowCount),
						CommonFunctions.getTestData(FILEPATH_TESTDATA, "CurrentZip", ID,rowCount),		
								CommonFunctions.getTestData(FILEPATH_TESTDATA, "NoofYrsCurrentAddress", ID,rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "email", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "HomePhone", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "MobilePhone", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "DateofBirth", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "SSN", ID, rowCount),
	CommonFunctions.getTestData(FILEPATH_TESTDATA, "MaritalStatus", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "NBS", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "NBSName", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "NBSPhone", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "NBSdob", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "NBSRel", ID, rowCount));
			

		return rs;
		
		
		
		
		
	}
}