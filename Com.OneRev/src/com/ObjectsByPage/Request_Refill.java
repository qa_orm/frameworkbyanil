package com.ObjectsByPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class Request_Refill {

	public Request_Refill(WebDriver drv) {
		PageFactory.initElements(drv, this);
		}

	//Refill Button element 	
	@FindBy(xpath="//a[@id='refillButton']")
	private WebElement Refill;
				
	public WebElement Refill_Button()
	{
		return Refill;
	}
	
	//1st Rx Number element 	
	@FindBy(xpath=".//*[@id='medications-table-body']/tr[1]/td[2]/a")
	private WebElement Rx;
					
	public WebElement Rx_Num()
	{
		return Rx;
	}

	//Submit Request element 	
	@FindBy(xpath="//a[@id='submitRefill']")
	private WebElement sub;
						
	public WebElement Sub_req()
	{
		return sub;
	}
	
	//I Understand element 	
	@FindBy(xpath="//button[@class='btn btn-primary']")
	private WebElement under;
							
	public WebElement Understand()
	{
		return under;
	}
	
	//FirstName element 	
	@FindBy(xpath="//input[@id='firstName']")
	private WebElement First;
								
	public WebElement FirstName()
	{
		return First;
	}
	
	//LastName element 	
	@FindBy(xpath="//input[@id='lastName']")
	private WebElement Last;
									
	public WebElement LastName()
	{
		return Last;
	}
	
	//Birth Date element 	
	@FindBy(xpath="//input[@id='birthDay']")
	private WebElement birth;
										
	public WebElement Birth_Date()
	{
		return birth;
	}
	
	//Delivery Date element 	
	@FindBy(xpath="//button[@class='ui-datepicker-trigger btn btn-info']")
	private WebElement delivery;
												
	public WebElement Delivery_Date()
	{
		return delivery;
	}
	
	//Date Selection element 	
	@FindBy(xpath=".//*[@id='ui-datepicker-div']/table/tbody/tr[4]/td[4]/a")
	private WebElement Select;
													
	public WebElement Date_Selection()
	{
		return Select;
	}
	
	//days supply of medication  	
	@FindBy(xpath="//input[@id='daysRemaining']")
	private WebElement supply;
														
	public WebElement days_supply()
	{
		return supply;
	}
	
   //changes in medications  	
	@FindBy(xpath=".//*[@id='noMedicationChanges']")
	private WebElement changes;
														
	public WebElement changes_Medica()
	{
		return changes;
	}
	
	//any new food, drug, environmental, or latex allergies  	
	@FindBy(xpath=".//*[@id='noChangesToNewAllergies']")
	private WebElement allergies;
															
	public WebElement latex_allergies()
	{
		return allergies;
	}
	
	//questions for the pharmacist  	
	@FindBy(xpath=".//*[@id='noQuestions']")
	private WebElement pharm;
																
	public WebElement pharmacist()
	{
		return pharm;
	}
	
	//health nurse administer  	
	@FindBy(xpath=".//*[@id='noNurseAdmin']")
	private WebElement nurse;
																	
	public WebElement administer()
	{
		return nurse;
	}
	
	
	//additional comments  	
	@FindBy(xpath=".//*[@id='noAdditionalComments']")
	private WebElement addi;
																		
	public WebElement additional()
	{
		return addi;
	}
	
	//Last four digits 	
	@FindBy(xpath="//input[@id='lastFourDigits']")
	private WebElement four;
																			
	public WebElement Lastfour_digits()
	{
		return four;
	}
	
	//Expiration Date Month 	
	@FindBy(xpath="//select[@id='cardExpMonth']")
	private WebElement month;
																				
	public WebElement Month(String mon)
	{
		Select stat=new Select(month);
	    stat.selectByVisibleText(mon);
	    return month;
	}
	
	//Expiration year 	
	@FindBy(xpath="//select[@id='cardExpYear']")
	private WebElement year;
																					
	public WebElement Year(String yea)
	{
		Select stat=new Select(year);
	    stat.selectByVisibleText(yea);
	    return year;
	}
	
	//Amount not to exceed 	
	@FindBy(xpath="//input[@id='creditCardLimit']")
	private WebElement Amo;
																				
	public WebElement Amount()
	{
		return Amo;
	}
	
	//I Accept the terms 	
	@FindBy(xpath="//button[@id='terms']")
	private WebElement terms;
																					
	public WebElement Accept()
	{
		return terms;
	}
	
	//Continue and Review 	
	@FindBy(xpath="//button[@id='continueButton']")
	private WebElement Cont;
																						
	public WebElement Continue()
	{
		return Cont;
	}
	
	//Submit Request	
	@FindBy(xpath="//button[@class='btn btn-success btn-block'][2]")
	private WebElement Request;
																							
	public WebElement Submit_Request()
	{
		return Request;
	}

	//Thank You Message	
	@FindBy(xpath="//h1[text()='Thank You']")
	private WebElement Message;
																								
	public WebElement Thank_You()
	{
		return Message;
	}
}
