package com.OneReverse;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.DriverScript.MasterScript;
import com.UtilityFiles.FPath;
import com.UtilityFiles.ReportStatus;

public class Loan {
	static com.UtilityFiles.CommonFunctions data;
	public static final String FILEPATH_TESTDATA = FPath.TESTDATA;
	public static final String ID = "com.OneReverse.Loan";
	ReportStatus rs = new ReportStatus();

	public ReportStatus Loan(Integer rowCount) throws Exception {

		rs.setReportStatus(false);
		// Load the selected Browser
		// PrimeClerk.loadUrl("Chrome");

		// Enter the login credentials from xl sheet.
		// PrimeClerk.login(data.getTestData(FILEPATH_TESTDATA, "UserName", ID),
		// data.getTestData(FILEPATH_TESTDATA, "Password", ID));

		OneRev.Loan(data.getTestData(FILEPATH_TESTDATA, "MortgageAppliedfor", ID, rowCount),
			//	data.getTestData(FILEPATH_TESTDATA, "PurposeofLoan", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "SelectedLoanPaymentPlan", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "EstimateofAppraisedValue", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "Margin", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "LoanOriginationFeeCalculationMethod", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "Thisapplicationwastakenby", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "LoanOriginatorsFirstName", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "LoanOriginatorsLastName", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "LoanOriginatorsPhoneNumber", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "LoanOriginationCompanysName", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "LoanOriginationCompanysAddress", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "LoanOriginationCompanysCity", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "LoanOriginationCompanysZipCode", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "Childrenlivingunderage", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "POA", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "NameofPOA", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "sqfootge", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "AltcontactName", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "AltcontactNamefulladdress", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "AltcontactNamephno", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "RelationshiptoAlternativeContact", ID, rowCount));

		
		return rs;

	}

	

}
