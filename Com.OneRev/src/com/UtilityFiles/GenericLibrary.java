package com.UtilityFiles;

import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import javax.imageio.ImageIO;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.common.usermodel.Hyperlink;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFHyperlink;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.DriverScript.MasterScript;

public class GenericLibrary {
	
	       public static int CaseCount;
	       public void testMain(Object[] args) throws IOException 
	       {
	                           
	              
	       }
	       /**
	       * To Return the Current Date and time stamp in 'MMM-dd-yyyy_h-mm-ss' format
	       * @param className
	       * @param Result
	       * @throws Exception
	       */
	       public static String getTimeStamp(){
	              Date d  = new Date();
	              SimpleDateFormat sdf = new SimpleDateFormat("MMM-dd-yyyy_h-mm-ss");
	              String date = sdf.format(d).toString();
	              return date;
	       }
	       
	       /**
	       * Create the Result Folder and returns the path
	       * @param className
	       * @param Result
	       * @throws Exception
	       */
	       public static String createResultFolder(){
	              File file = new File(FPath.RESULTS+MasterScript.ConstantTimeStamp);//C:/Navigator_Project/Navigator/Results/Result_"+MAIN.ConstantTimeStamp
	              if (!file.exists()) {
	                     if (file.mkdirs()) {
	                           
	                     } else {
	                           System.out.println("Failed to create Result Folder!");
	                     }
	              }
	              return FPath.RESULTS+MasterScript.ConstantTimeStamp;
	       }
	       
	       /**
	       * Create the Log Folder and returns the path
	       * @param className
	       * @param Result
	       * @throws Exception
	       */
	       public static String createLogFolder(){
	              System.out.println(MasterScript.ConstantTimeStamp);
	              File file = new File(FPath.LOGS+MasterScript.ConstantTimeStamp);
	              if (!file.exists()) {
	                     if (file.mkdir()) {
	                           //System.out.println("Log Directory is created!");
	                     } else {
	                           System.out.println("Failed to create Log directory!");
	                     }
	              }
	              return FPath.LOGS+MasterScript.ConstantTimeStamp;
	       }
	       
	       /**
	       * Create the ScreenShot Folder and returns the path
	       * @param className
	       * @param Result
	       * @throws Exception
	       */
	       public static String createScreenShotFolder() {
	              File file = new File(FPath.SCREENSHOT+MasterScript.ConstantTimeStamp);
	              if (!file.exists()) {
	                     if (file.mkdir()) {
	                           //System.out.println("Screenshot_Directory is created!");
	                     } else {
	                           System.out.println("Failed to create Screenshot directory!");
	                     }
	              }
	              return FPath.SCREENSHOT+MasterScript.ConstantTimeStamp;
	       }
	       
	       /**
	       * To Copy the Result_Template file from Template folder to Result folder and returns the path 
	        * @param className
	       * @param Result
	       * @throws Exception
	       */    
	       public static String copyFile(){
	              InputStream inStream = null;
	              OutputStream outStream = null;
	              try{
	                     File Originalfile =new File(FPath.RESULT_TEMPLATE);
	                     File Copyfile =new File(FPath.RESULTS+MasterScript.ConstantTimeStamp+"/"+"TestResult"+".xlsx");
	                     inStream = new FileInputStream(Originalfile);
	                     outStream = new FileOutputStream(Copyfile);
	                     byte[] buffer = new byte[1024];
	                     int length;
	                     //copy the file content in bytes 
	                     while ((length = inStream.read(buffer)) > 0){
	                           outStream.write(buffer, 0, length);
	                     }
	                     inStream.close();
	                     outStream.close();
	                     //System.out.println("Result template file updated successful!");
	                     return FPath.RESULTS+MasterScript.ConstantTimeStamp+"/"+"TestResult"+".xlsx";
	              }catch(IOException e){
	                     e.printStackTrace();
	                     return null;
	              }

	       }

	       /**
	       * To Create Latest current Time stamp folder under Logs File and returns the path 
	        * @param className
	       * @param Result
	       * @throws Exception
	       */    
	       public static String Create_LogFile(String scrptNm){
	              InputStream inStream = null;
	              OutputStream outStream = null;
	              try{
	                     String ScriptName =MasterScript.GlobalScript;
	                     File Originalfile =new File(FPath.TESTSTEP_TEMPLATE);
	                     File Copyfile =new File(ScriptName);
	                     System.out.println(Copyfile);
	                     inStream = new FileInputStream(Originalfile);
	                     outStream = new FileOutputStream(Copyfile);
	                     byte[] buffer = new byte[1024];
	                     int length;
	                     //copy the file content in bytes 
	                     while ((length = inStream.read(buffer)) > 0){
	                           outStream.write(buffer, 0, length);
	                     }
	                     inStream.close();
	                     outStream.close();
	                     MasterScript.Logpath =  FPath.LOGS+MasterScript.ConstantTimeStamp+"\\mytest02.xlsx";
	                                    // "C:/Navigator_Project/Navigator/Logs/Log_"+MAIN.ConstantTimeStamp+"/"+"mytest02"+".xlsx";
	                     System.out.println("LogFile create successful!");
	                     //return "C:/Navigator_Project/Navigator/Logs/Log_"+MAIN.ConstantTimeStamp+"/"+scrptNm+".xlsx";
	                       return FPath.LOGS+MasterScript.ConstantTimeStamp+"/"+"mytest02"+".xlsx";
	              }catch(IOException e){
	                     e.printStackTrace();
	                     return null;
	              }

	       }
	       
	       /**
	       * Updates the TestResult excel with Pass and Fail and adds the Link for LogFile
	       * @param className
	       * @param Result
	       * @throws Exception
	       */
	       @SuppressWarnings("deprecation")
	       public static void setResultCell(String className, String Result) throws Exception{
	              XSSFSheet ExcelWSheet;
	              XSSFWorkbook ExcelWBook;
	              XSSFCell Cell;
	              XSSFRow Row;
	              XSSFCellStyle Style;
	              FileInputStream ExcelFile = new FileInputStream(FPath.RESULTS+MasterScript.ConstantTimeStamp+"/"+"TestResult"+".xlsx");
	              // Access the required test data sheet
	              ExcelWBook = new XSSFWorkbook(ExcelFile);
	              ExcelWSheet = ExcelWBook.getSheetAt(0);
	              //System.out.println("Set excel2 success");
	              Integer rowCount = ExcelWSheet.getLastRowNum()+1;
	              //System.out.println("Last row of Result File: " + rowCount);
	              if(Result.equalsIgnoreCase("pass")){ // If PASS
	                     try{
	                           Row = ExcelWSheet.createRow(rowCount);
	                           Cell = Row.getCell(0, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                           if (Cell == null) {
	                                  Cell = Row.createCell(0);
	                                  Cell.setCellValue(rowCount.toString());
	                           } else {
	                                  Cell.setCellValue(rowCount.toString());
	                           }
	                           Cell = Row.getCell(1, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                           if (Cell == null) {
	                                  Cell = Row.createCell(1);
	                                  Cell.setCellValue(className);
	                           } else {
	                                  Cell.setCellValue(className);
	                           }
	                           Cell = Row.getCell(2, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                           if (Cell == null) {
	                                  Cell = Row.createCell(2);
	                                  Style = ExcelWBook.createCellStyle();
	                                  Style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	                                  Style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	                                  Cell.setCellValue(Result);
	                                  Cell.setCellStyle(Style);
	                           } else {
	                                  Style = ExcelWBook.createCellStyle();
	                                  Style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	                                  Style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	                                  Cell.setCellValue(Result);
	                                  Cell.setCellStyle(Style);
	                           }// Constant variables Test Data path and Test Data file name
	                           FileOutputStream fileOut = new FileOutputStream(FPath.RESULTS+MasterScript.ConstantTimeStamp+"/"+"TestResult"+".xlsx");
	                           ExcelWBook.write(fileOut);
	                           fileOut.flush();
	                           fileOut.close();
	                     }catch(Exception e){
	                           throw (e);
	                     }
	              }

	              else{ // if FAIL 

	                     Row = ExcelWSheet.createRow(rowCount);
	                     Cell = Row.getCell(0, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                     if (Cell == null) {
	                           Cell = Row.createCell(0);
	                           Cell.setCellValue(rowCount.toString());
	                     } else {
	                           Cell.setCellValue(rowCount.toString());
	                     }
	                     Cell = Row.getCell(1, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                     if (Cell == null) {
	                           Cell = Row.createCell(1);
	                           Cell.setCellValue(className);
	                     } else {
	                           Cell.setCellValue(className);
	                     }
	                     Cell = Row.getCell(2, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                     if (Cell == null) {
	                           Cell = Row.createCell(2);
	                           Style = ExcelWBook.createCellStyle();
	                           Style.setFillForegroundColor(IndexedColors.RED.getIndex());
	                           Style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	                           Cell.setCellValue(Result);
	                           Cell.setCellStyle(Style);
	                     } else {
	                           Style = ExcelWBook.createCellStyle();
	                           Style.setFillForegroundColor(IndexedColors.RED.getIndex());
	                           Style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	                           Cell.setCellValue(Result);
	                           Cell.setCellStyle(Style);
	                     }
	              }
	              String ScriptName =MasterScript.GlobalScript;
	              //String GlobalScript="C:/Framework/SalesForce/Logs/Log_"+ConstantTimeStamp+"/"+scrptNm+".xlsx";
	              String Log_ExcelLink=ScriptName ;

	              Cell = Row.getCell(3, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	              if (Cell == null) {
	                     Cell = Row.createCell(3);
	                     CellStyle hlink_style = ExcelWBook.createCellStyle();
	                     XSSFFont hlink_font = ExcelWBook.createFont();
	                     //hlink_font.setUnderline(Font.U_SINGLE);
	                     hlink_font.setColor(IndexedColors.BLUE.getIndex());
	                     hlink_style.setFont(hlink_font);
	                     XSSFCreationHelper helper= ExcelWBook.getCreationHelper();
	                     XSSFHyperlink FileLink  = helper.createHyperlink(Hyperlink.LINK_FILE);
	                     Cell.setCellValue("Log_File");
	                     FileLink.setAddress(Log_ExcelLink);
	                     Cell.setHyperlink(FileLink);
	                     Cell.setCellStyle(hlink_style);
	              } else {
	                     CellStyle hlink_style = ExcelWBook.createCellStyle();
	                     XSSFFont hlink_font = ExcelWBook.createFont();
	                     //hlink_font.setUnderline(Font.U_SINGLE);
	                     hlink_font.setColor(IndexedColors.BLUE.getIndex());
	                     hlink_style.setFont(hlink_font);
	                     XSSFCreationHelper helper= ExcelWBook.getCreationHelper();
	                     XSSFHyperlink FileLink  = helper.createHyperlink(Hyperlink.LINK_FILE);
	                     Cell.setCellValue(Log_ExcelLink);
	                     FileLink.setAddress(Log_ExcelLink);
	                     Cell.setHyperlink(FileLink);
	                     Cell.setCellStyle(hlink_style);
	              }


	              FileOutputStream fileOut = new FileOutputStream(FPath.RESULTS+MasterScript.ConstantTimeStamp+"/"+"TestResult"+".xlsx");
	              ExcelWBook.write(fileOut);
	              fileOut.flush();
	              fileOut.close();
	       }
	       
	       /**
	       * Updates the Log excel with Pass and Fail and adds the Link for Error ScreenShot 
	        * @param className
	       * @param Result
	       * @throws Exception
	       */
	       @SuppressWarnings("deprecation")
	       public static void setCellData(String desc,String acp, String asp, String Result) throws Exception    
	       {   
	              XSSFSheet ExcelWSheet;
	              XSSFWorkbook ExcelWBook;
	              XSSFCell Cell;
	              XSSFRow Row;
	              XSSFCellStyle Style;
	              String idTester;
	              //File f = new File("C:\\Users\\javeed.sheikh\\Desktop\\xyz.xlsx");
	              String ScriptName =MasterScript.GlobalScript;
	              //System.out.println(ScriptName);
	              FileInputStream ExcelFile = new FileInputStream(ScriptName);
	              //FileInputStream ExcelFile = new FileInputStream(f); 
	              // Access the required test data sheet
	              ExcelWBook = new XSSFWorkbook(ExcelFile);
	              ExcelWSheet = ExcelWBook.getSheet("sheet1");
	              int rowNo=ExcelWSheet.getLastRowNum();
	              System.out.println("row no..: "+rowNo);
	              if(rowNo==0){
	                     //idTester=DemoMainTester.testId;
	                     //VIVEK UPDATED START...........................................
	                     
	                      try{
	                                  System.out.println("------>Test Step :: "+desc+" :: "+Result + " >RowCount: "+0);
	                                  Row = ExcelWSheet.createRow(0);
	                                  Cell = Row.getCell(0, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                  if (Cell == null) {
	                                         Cell = Row.createCell(0);
	                                         Cell.setCellValue("TestcaseID");
	                                  } else {
	                                         Cell.setCellValue("TestcaseID");
	                                  }
	                                  
	                                  Cell = Row.getCell(1, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                  if (Cell == null) {
	                                         Cell = Row.createCell(1);
	                                         Cell.setCellValue("Description");
	                                  } else {
	                                         Cell.setCellValue("Description");
	                                  }
	                                  Cell = Row.getCell(2, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                  if (Cell == null) {
	                                         Cell = Row.createCell(2);
	                                         Cell.setCellValue("Expected Result");
	                                  } else {
	                                         Cell.setCellValue("Expected Result");
	                                  }
	                                  Cell = Row.getCell(3, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                  if (Cell == null) {
	                                         Cell = Row.createCell(3);
	                                         Cell.setCellValue("Actual Result");
	                                  } else {
	                                         Cell.setCellValue("Actual Result");
	                                  }
	                                  Cell = Row.getCell(4, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                  if (Cell == null) {
	                                         Cell = Row.createCell(4);
	                                                
	                                         Cell.setCellValue("Status");
	                                         
	                                  } else {
	                                                
	                                         Cell.setCellValue("Status");
	                                         
	                                  }// Constant variables Test Data path and Test Data file name
	                                  FileOutputStream fileOut = new FileOutputStream(ScriptName);
	                                  //FileOutputStream fileOut = new FileOutputStream(f);
	                                  ExcelWBook.write(fileOut);
	                                  fileOut.flush();
	                                  fileOut.close();
	                           }catch(Exception e){
	                                  throw (e);
	                           }
	                     
	                      //VIVEK UPDATED END.................................
	              Integer rowCount = ExcelWSheet.getLastRowNum()+1;
	              //System.out.println("------>Test Step :: "+desc+" :: "+Result + " >RowCount: "+rowCount);
	              if(Result.equalsIgnoreCase("pass")){
	                     try{
	                           System.out.println("------>Test Step :: "+desc+" :: "+Result + " >RowCount: "+rowCount);
	                           Row = ExcelWSheet.createRow(rowCount);
	                           Cell = Row.getCell(0, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                           if (Cell == null) {
	                                  Cell = Row.createCell(0);
	                                  Cell.setCellValue(MasterScript.scriptName);
	                                  //Cell.setCellValue(DemoMainTester.testId);
	                           } else {
	                                  Cell.setCellValue(MasterScript.scriptName);
	                                  //Cell.setCellValue(DemoMainTester.testId);
	                           }
	                           
	                           Cell = Row.getCell(1, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                           if (Cell == null) {
	                                  Cell = Row.createCell(1);
	                                  Cell.setCellValue(desc);
	                           } else {
	                                  Cell.setCellValue(desc);
	                           }
	                           Cell = Row.getCell(2, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                           if (Cell == null) {
	                                  Cell = Row.createCell(2);
	                                  Cell.setCellValue(acp);
	                           } else {
	                                  Cell.setCellValue(acp);
	                           }
	                           Cell = Row.getCell(3, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                           if (Cell == null) {
	                                  Cell = Row.createCell(3);
	                                  Cell.setCellValue(asp);
	                           } else {
	                                  Cell.setCellValue(asp);
	                           }
	                           Cell = Row.getCell(4, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                           if (Cell == null) {
	                                  Cell = Row.createCell(4);
	                                         Style = ExcelWBook.createCellStyle();
	                                  Style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	                                  Style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	                                  Cell.setCellValue(Result);
	                                  Cell.setCellStyle(Style);
	                           } else {
	                                         Style = ExcelWBook.createCellStyle();
	                                  Style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	                                  Style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	                                  Cell.setCellValue(Result);
	                                  Cell.setCellStyle(Style);
	                           }// Constant variables Test Data path and Test Data file name
	                           FileOutputStream fileOut = new FileOutputStream(ScriptName);
	                           //FileOutputStream fileOut = new FileOutputStream(f);
	                           ExcelWBook.write(fileOut);
	                           fileOut.flush();
	                           fileOut.close();
	                     }catch(Exception e){
	                           throw (e);
	                     }
	              } 
	       
	              else if(Result.equalsIgnoreCase("fail")){
	                     String screenShotLink =snapShot("Error.jpg",MasterScript.ScreenShotPath);
	                     try{
	                           System.err.println("------>Test Step :: "+desc+" :: "+Result + " >RowCount: "+rowCount);
	                                  Row = ExcelWSheet.createRow(rowCount);
	                           Cell = Row.getCell(0, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                           if (Cell == null) {
	                                  Cell = Row.createCell(0);
	                                  Cell.setCellValue(MasterScript.scriptName);
	                                  //Cell.setCellValue(DemoMainTester.testId);
	                           } else {
	                                  Cell.setCellValue(MasterScript.scriptName);
	                                  //Cell.setCellValue(DemoMainTester.testId);
	                           }
	                           Cell = Row.getCell(1, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                           if (Cell == null) {
	                                  Cell = Row.createCell(1);
	                                  Cell.setCellValue(desc);
	                           } else {
	                                  Cell.setCellValue(desc);
	                           }
	                           Cell = Row.getCell(2, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                           if (Cell == null) {
	                                  Cell = Row.createCell(2);
	                                  Cell.setCellValue(acp);
	                           } else {
	                                  Cell.setCellValue(acp);
	                           }
	                           Cell = Row.getCell(3, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                           if (Cell == null) {
	                                  Cell = Row.createCell(3);
	                                  Cell.setCellValue(asp);
	                           } else {
	                                  Cell.setCellValue(asp);
	                           }
	                           Cell = Row.getCell(4, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                           if (Cell == null) {
	                                  Cell = Row.createCell(4);
	                                         Style = ExcelWBook.createCellStyle();
	                                  Style.setFillForegroundColor(IndexedColors.RED.getIndex());
	                                  Style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	                                  Cell.setCellValue(Result);
	                                  Cell.setCellStyle(Style);
	                           } else {
	                                         Style = ExcelWBook.createCellStyle();
	                                  Style.setFillForegroundColor(IndexedColors.RED.getIndex());
	                                  Style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	                                  Cell.setCellValue(Result);
	                                  Cell.setCellStyle(Style);
	                           }
	                           Cell = Row.getCell(5, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                           if (Cell == null) {
	                                  Cell = Row.createCell(5);
	                                  CellStyle hlink_style = ExcelWBook.createCellStyle();
	                                  XSSFFont hlink_font = ExcelWBook.createFont();
	                                  //hlink_font.setUnderline(Font.U_SINGLE);
	                                  hlink_font.setColor(IndexedColors.BLUE.getIndex());
	                                  hlink_style.setFont(hlink_font);
	                                  XSSFCreationHelper helper= ExcelWBook.getCreationHelper();
	                                  XSSFHyperlink FileLink  = helper.createHyperlink(Hyperlink.LINK_FILE);
	                                  Cell.setCellValue("ScreenShot");
	                                  FileLink.setAddress(screenShotLink);
	                                  Cell.setHyperlink(FileLink);
	                                  Cell.setCellStyle(hlink_style);
	                           } else {
	                                  CellStyle hlink_style = ExcelWBook.createCellStyle();
	                                  XSSFFont hlink_font = ExcelWBook.createFont();
	                                  //hlink_font.setUnderline(Font.U_SINGLE);
	                                  hlink_font.setColor(IndexedColors.BLUE.getIndex());
	                                  hlink_style.setFont(hlink_font);
	                                  XSSFCreationHelper helper= ExcelWBook.getCreationHelper();
	                                  XSSFHyperlink FileLink  = helper.createHyperlink(Hyperlink.LINK_FILE);
	                                  Cell.setCellValue(screenShotLink);
	                                  FileLink.setAddress(screenShotLink);
	                                  Cell.setHyperlink(FileLink);
	                                  Cell.setCellStyle(hlink_style);
	                           }
	                           FileOutputStream fileOut = new FileOutputStream(ScriptName);
	                           //FileOutputStream fileOut = new FileOutputStream(f);
	                           ExcelWBook.write(fileOut);
	                           fileOut.flush();
	                           fileOut.close();
	                     }catch(Exception e){
	                           throw (e);
	                     }
	              }//elseif
	              //else{}
	       }//if
	              //else(){}
	              // else if( testId.equals(DemoMainTester.testId))
	              else{

	                           Integer rowCount = ExcelWSheet.getLastRowNum()+1;
	                           //System.out.println("------>Test Step :: "+desc+" :: "+Result + " >RowCount: "+rowCount);
	                           if(Result.equalsIgnoreCase("pass")){
	                                  try{
	                                         System.out.println("------>Test Step :: "+desc+" :: "+Result + " >RowCount: "+rowCount);
	                                         Row = ExcelWSheet.createRow(rowCount);
	                                         Cell = Row.getCell(0, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                         if (Cell == null) {
	                                                Cell = Row.createCell(0);
	                                                //Cell.setCellValue(DemoMainTester.testId);
	                                                Cell.setCellValue(MasterScript.scriptName);
	                                         } else {
	                                                //Cell.setCellValue(DemoMainTester.testId);
	                                                Cell.setCellValue(MasterScript.scriptName);
	                                         }
	                                         
	                                         Cell = Row.getCell(1, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                         if (Cell == null) {
	                                                Cell = Row.createCell(1);
	                                                Cell.setCellValue(desc);
	                                         } else {
	                                                Cell.setCellValue(desc);
	                                         }
	                                         Cell = Row.getCell(2, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                         if (Cell == null) {
	                                                Cell = Row.createCell(2);
	                                                Cell.setCellValue(acp);
	                                         } else {
	                                                Cell.setCellValue(acp);
	                                         }
	                                         Cell = Row.getCell(3, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                         if (Cell == null) {
	                                                Cell = Row.createCell(3);
	                                                Cell.setCellValue(asp);
	                                         } else {
	                                                Cell.setCellValue(asp);
	                                         }
	                                         Cell = Row.getCell(4, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                         if (Cell == null) {
	                                                Cell = Row.createCell(4);
	                                                       Style = ExcelWBook.createCellStyle();
	                                                Style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	                                                Style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	                                                Cell.setCellValue(Result);
	                                                Cell.setCellStyle(Style);
	                                         } else {
	                                                       Style = ExcelWBook.createCellStyle();
	                                                Style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	                                                Style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	                                                Cell.setCellValue(Result);
	                                                Cell.setCellStyle(Style);
	                                         }// Constant variables Test Data path and Test Data file name
	                                         FileOutputStream fileOut = new FileOutputStream(ScriptName);
	                                         //FileOutputStream fileOut = new FileOutputStream(f);
	                                         ExcelWBook.write(fileOut);
	                                         fileOut.flush();
	                                         fileOut.close();
	                                  }catch(Exception e){
	                                         throw (e);
	                                  }
	                           } 
	                           else if(Result.equalsIgnoreCase("fail")){
	                                  String screenShotLink =snapShot("Error.jpg",MasterScript.ScreenShotPath);
	                                  try{
	                                         System.err.println("------>Test Step :: "+desc+" :: "+Result + " >RowCount: "+rowCount);
	                                                Row = ExcelWSheet.createRow(rowCount);
	                                         Cell = Row.getCell(0, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                         if (Cell == null) {
	                                                Cell = Row.createCell(0);
	                                                //Cell.setCellValue(DemoMainTester.testId);
	                                                Cell.setCellValue(MasterScript.scriptName);
	                                         } else {
	                                                Cell.setCellValue(MasterScript.scriptName);
	                                                //Cell.setCellValue(DemoMainTester.testId);
	                                         }
	                                         Cell = Row.getCell(1, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                         if (Cell == null) {
	                                                Cell = Row.createCell(1);
	                                                Cell.setCellValue(desc);
	                                         } else {
	                                                Cell.setCellValue(desc);
	                                         }
	                                         Cell = Row.getCell(2, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                         if (Cell == null) {
	                                                Cell = Row.createCell(2);
	                                                Cell.setCellValue(acp);
	                                         } else {
	                                                Cell.setCellValue(acp);
	                                         }
	                                         Cell = Row.getCell(3, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                         if (Cell == null) {
	                                                Cell = Row.createCell(3);
	                                                Cell.setCellValue(asp);
	                                         } else {
	                                                Cell.setCellValue(asp);
	                                         }
	                                         Cell = Row.getCell(4, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                         if (Cell == null) {
	                                                Cell = Row.createCell(4);
	                                                       Style = ExcelWBook.createCellStyle();
	                                                Style.setFillForegroundColor(IndexedColors.RED.getIndex());
	                                                Style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	                                                Cell.setCellValue(Result);
	                                                Cell.setCellStyle(Style);
	                                         } else {
	                                                       Style = ExcelWBook.createCellStyle();
	                                                Style.setFillForegroundColor(IndexedColors.RED.getIndex());
	                                                Style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	                                                Cell.setCellValue(Result);
	                                                Cell.setCellStyle(Style);
	                                         }
	                                         Cell = Row.getCell(5, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                         if (Cell == null) {
	                                                Cell = Row.createCell(5);
	                                                CellStyle hlink_style = ExcelWBook.createCellStyle();
	                                                XSSFFont hlink_font = ExcelWBook.createFont();
	                                                //hlink_font.setUnderline(Font.U_SINGLE);
	                                                hlink_font.setColor(IndexedColors.BLUE.getIndex());
	                                                hlink_style.setFont(hlink_font);
	                                                XSSFCreationHelper helper= ExcelWBook.getCreationHelper();
	                                                XSSFHyperlink FileLink  = helper.createHyperlink(Hyperlink.LINK_FILE);
	                                                Cell.setCellValue("ScreenShot");
	                                                FileLink.setAddress(screenShotLink);
	                                                Cell.setHyperlink(FileLink);
	                                                Cell.setCellStyle(hlink_style);
	                                         } else {
	                                                CellStyle hlink_style = ExcelWBook.createCellStyle();
	                                                XSSFFont hlink_font = ExcelWBook.createFont();
	                                                //hlink_font.setUnderline(Font.U_SINGLE);
	                                                hlink_font.setColor(IndexedColors.BLUE.getIndex());
	                                                hlink_style.setFont(hlink_font);
	                                                XSSFCreationHelper helper= ExcelWBook.getCreationHelper();
	                                                XSSFHyperlink FileLink  = helper.createHyperlink(Hyperlink.LINK_FILE);
	                                                Cell.setCellValue(screenShotLink);
	                                                FileLink.setAddress(screenShotLink);
	                                                Cell.setHyperlink(FileLink);
	                                                Cell.setCellStyle(hlink_style);
	                                         }
	                                         FileOutputStream fileOut = new FileOutputStream(ScriptName);
	                                         //FileOutputStream fileOut = new FileOutputStream(f);
	                                         ExcelWBook.write(fileOut);
	                                         fileOut.flush();
	                                         fileOut.close();
	                                  }catch(Exception e){
	                                         throw (e);
	                                  }
	                           }//elseif
	                           else{

	                                  try{
	                                         System.out.println("------>Test Step :: "+desc+" :: "+Result + " >RowCount: "+rowCount);
	                                         Row = ExcelWSheet.createRow(rowCount);
	                                         /* Cell = Row.getCell(0, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                         if (Cell == null) {
	                                                Cell = Row.createCell(0);
	                                                Cell.setCellValue("");
	                                         } else {
	                                                Cell.setCellValue("");
	                                         }
	                                         Cell = Row.getCell(1, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                         if (Cell == null) {
	                                                Cell = Row.createCell(1);
	                                                Cell.setCellValue(desc);
	                                         } else {
	                                                Cell.setCellValue(desc);
	                                         }
	                                         Cell = Row.getCell(2, org.apache.poi.ss.usermodel.Row.RETURN_BLANK_AS_NULL);
	                                         if (Cell == null) {
	                                                Cell = Row.createCell(2);
	                                                       Style = ExcelWBook.createCellStyle();
	                                                Style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	                                                Style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	                                                Cell.setCellValue("");
	                                                //Cell.setCellStyle(Style);
	                                         } else {
	                                                       Style = ExcelWBook.createCellStyle();
	                                                Style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	                                                Style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	                                                Cell.setCellValue("");
	                                                //Cell.setCellStyle(Style);
	                                         }*/// Constant variables Test Data path and Test Data file name
	                                         FileOutputStream fileOut = new FileOutputStream(ScriptName);
	                                         //FileOutputStream fileOut = new FileOutputStream(f);
	                                         ExcelWBook.write(fileOut);
	                                         fileOut.flush();
	                                         fileOut.close();
	                                  }catch(Exception e){
	                                         throw (e);
	                                  }
	                           
	                           }
	                     
	              }//else if
	              
	       }
	       /**
	       * To Read Test data from DataTable file
	       * @param className
	       * @param Result
	       * @throws Exception
	       */
	       public static String []  ReadTestdata(String filePath) throws Exception {
	              FileInputStream fileInputStream = new FileInputStream(filePath);
	              Workbook wb = WorkbookFactory.create(fileInputStream);
	              for (int x=0;x<wb.getNumberOfSheets();x++) {
	              Sheet sheet =  wb.getSheetAt(x);
	              String sh = wb.getSheetName(x);
	              sheet = wb.getSheetAt(0);
	              Sheet s = wb.getSheet(sh);
	      	    Iterator<Row> rowIterator = sheet.iterator();
	      	    while (rowIterator.hasNext()) {
	      	      Row row = rowIterator.next();
	      	      Iterator <Cell> cellIterator = row.cellIterator();
	      	      while (cellIterator.hasNext()) {
	      	        Cell cell = cellIterator.next();
	      	        System.out.print(cell.getStringCellValue() + "\t\t");
	      	      }
	      	    }
	              
	              
	              
	            //  org.apache.poi.ss.usermodel.Sheet s =  wb.getSheet("Sheet1");
	              int rowCnt = ((org.apache.poi.ss.usermodel.Sheet) s).getLastRowNum();
	              org.apache.poi.ss.usermodel.Row row = null; 
	              row = ((org.apache.poi.ss.usermodel.Sheet) s).getRow(1);
	              org.apache.poi.ss.usermodel.Cell cell;
	        int cols = ((org.apache.poi.ss.usermodel.Sheet) s).getRow(1).getPhysicalNumberOfCells();
	       java.util.List list =new ArrayList();
	              String [] values = new String [cols];
	              for(int c = 0; c < cols; c++) {
	           cell = ((org.apache.poi.ss.usermodel.Row) row).getCell((short)c);
	            if(cell != null) {
	                 cell = row.getCell(c);                                                              
	                values[c] = cell.getRichStringCellValue().toString();
	               }
	        }
	              
	              
	              return values;
	              }
				return null;
	       }

	       /**
	       * To Create HTML Report
	       * @param className
	       * @param Result
	       * @throws Exception
	       */
	       
	       public static void CreateHTMLReport(String SuiteDuration,String ConstantTimeStamp,String SuiteStart) throws Exception{
	        String cont=null; 
	        String cont1=null;
	        String cont2=null;
	        String cont3=null;
	        String cont4=null;
	        String cont5=null;
	        File fi1=null;
	        //if (CaseCount==0) {
	        
	        //cont="<HTML><HEAD><TITLE>Navigator Test Report</TITLE><META http-equiv=Content-Type content=text/html; charset=windows-1252><META content=MSHTML 6.00.2900.3268 name=GENERATOR></HEAD><H2 align=center><U><FONT color=#187030><B>Framework Consolidated Test Report</B></FONT> <U></H2><HR><TABLE width=100% height=37 border=0 align=center><TBODY><TR><TD height=19 <FONT color=black><B>Project Name :: </B></FONT><FONT color=#187030><B>Navigator</B></FONT><TD width=50% align=right><div align=left><FONT color=black><B>Date and Time of Execution : </B></FONT><FONT color=#187030><B>"+SuiteStart+"</B></FONT></div></TD></TR>";
	        cont="<HTML><HEAD><img src='D:\\FrameWork\\Logo\\ORMLogo.png' alt='logo' /><TITLE>ORM Test Report</TITLE><META http-equiv=Content-Type content=text/html; charset=windows-1252><META content=MSHTML 6.00.2900.3268 name=GENERATOR></HEAD><H2 align=center><U><FONT color=##FFA500><B>Automation Execution Report</U></B></FONT> <U></H2><HR><TABLE width=100% height=37 border=0 align=center><TBODY><TR><TD height=19 <FONT color=black><B>Project Name : </B></FONT><FONT color=#E87722><B>One Reverse Mortgage</B></FONT><TD width=30% align=right><div align=left><FONT color=black><B>Date and Time of Execution : </B></FONT><FONT color=#E87722><B>"+SuiteStart+"</B></FONT></div></TD></TR></U>";
	         fi1=new File("D:\\FrameWork\\Results\\Result_"+MasterScript.ConstantTimeStamp+"\\Execution_Report.html");
	       // }
	         
	         //<style>body{background-color: #E87722}</style><body></body>
	        
	  FileOutputStream fos=new FileOutputStream(fi1);
	  fos.write(cont.getBytes());
	  
	  int passCount = 0;
	  int failCount = 0;
	  FileInputStream fileInputStream = new FileInputStream("D:\\FrameWork\\Results\\Result_"+MasterScript.ConstantTimeStamp+"\\TestResult.xlsx");
	  //FileInputStream fileInputStream = new FileInputStream(MAIN.Logpath);
	        Workbook wb = WorkbookFactory.create(fileInputStream);
	        
	        org.apache.poi.ss.usermodel.Sheet s =  wb.getSheet("Sheet1");
	        int rowCnt = ((org.apache.poi.ss.usermodel.Sheet) s).getLastRowNum();
	        org.apache.poi.ss.usermodel.Row row = null; 
	        for(int j=1;j<=rowCnt;j++){
	               row = s.getRow(j);
	               org.apache.poi.ss.usermodel.Cell cell = row.getCell(2); 
	               String testCasename = cell.getRichStringCellValue().toString();
	               if(testCasename.equalsIgnoreCase("Pass")){
	                     passCount++;
	               }
	               if(testCasename.equalsIgnoreCase("fail")){
	                     failCount++;
	               }
	               
	        }
	        //#ffffff
	        cont="<TR bgColor=#ffffff><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD></TR><TD><B><FONT></FONT></B></TD></TR></TBODY></TABLE><BR><BR><HR><TABLE width=80% align=center border=1><TBODY><TR bgColor=#E87722><TD align=middle><B><FONT color=#FFFDD0>Total Testcases </FONT></B></TD><TD align=middle><B><FONT color=#FFFDD0>No. of TC Passed </FONT></B></TD><TD align=middle><B><FONT color=#FFFDD0>No. of TC Failed </FONT></B></TD><TD align=middle><B><FONT color=#FFFDD0>Total Time Elapsed</FONT></B></TD></TR><TR bgColor=#ffffff>";
	  fos.write(cont.getBytes());
	        
	  cont="<TD align=middle><B>"+rowCnt+" </B></TD>";
	  fos.write(cont.getBytes());
	  cont="<TD align=middle><B>"+passCount+" </B></TD>";
	  fos.write(cont.getBytes());
	  cont="<TD align=middle><B><FONT color=#ff0066>"+failCount+"</FONT>";
	  fos.write(cont.getBytes());
	  cont="<TD align=middle><B>"+SuiteDuration+" </B></TD>";
	  fos.write(cont.getBytes());
	     
	  //<TR bgColor=#E87722><TD></TD><TD></TD><TD></TD><TD></TD><TD></TD></TR><TD><B><FONT></FONT></B></TD></TR>
	  cont="</TBODY></TABLE><BR><BR><HR><H3 align=center><FONT color=black><U><B>Summary of Consolidated Test Report </U> </B></FONT></H3></TR><TR bgColor=#E87722>";
	  fos.write(cont.getBytes());
	  
	  
	     cont="<TABLE width=100% align=center border=1><TBODY><TR bgColor=#E87722><TD><FONT color=#FFFDD0><B>TestCase Name</B></FONT></TD><TD width=10%><FONT color=#FFFDD0><B>Execution Status</B></FONT></TD>";
	    fos.write(cont.getBytes());
	     
	 fileInputStream = new FileInputStream("D:\\FrameWork\\Results\\Result_"+MasterScript.ConstantTimeStamp+"\\TestResult.xlsx");
	    // fileInputStream = new FileInputStream(MAIN.Logpath);
	     
	        wb = WorkbookFactory.create(fileInputStream);
	        //s =  wb.getSheet("Result");
	        s =  wb.getSheet("Sheet1");
	        rowCnt = ((org.apache.poi.ss.usermodel.Sheet) s).getLastRowNum();
	        row = null; 
	        //#FF0000 
	        for(int j=1;j<=rowCnt;j++){
	               row = s.getRow(j);
	               org.apache.poi.ss.usermodel.Cell cell = row.getCell(1); 
	               String testCasename = cell.getRichStringCellValue().toString();
	               cell = row.getCell(2); 
	               String Sts = cell.getRichStringCellValue().toString();
	               if (Sts.equalsIgnoreCase("Fail")){
	                     cont="<TR bgColor=#ffffff>";
	                     
	               }else{
	                     cont="<TR bgColor=#ffffff>";
	                     
	               }
	                     
	                                   
	               
	      fos.write(cont.getBytes());
	        cont="<TD>"+testCasename+"</TD>";
	        if (Sts.equalsIgnoreCase("Fail")){
	              cont1="<TD align=middle><B><FONT color=#FF0000 size=3>"+Sts.toUpperCase()+"</B></TD>";
	        }else{
	              cont1="<TD align=middle><B><FONT color=#006400 size=3>"+Sts.toUpperCase()+"</B></TD>";
	        }
	        fos.write(cont.getBytes());
	     fos.write(cont1.getBytes());
	        }
	        //cont="</B></TD></TR></TBODY></TABLE><BR><BR><BR><BR><BR><BR><TABLE width=100% align=center bgColor=#E87722 border=0><TBODY><TR><TD align=middle><FONT face=Verdana, Arial color=#ffffff size=1></FONT></TD><TD align=right><FONT face=Verdana, Arial color=#ffffff size=1></FONT><footer> <img src='C:\\Navigator_Project\\Galaxe.png' /> </footer></TD></TR></TBODY></TABLE></U></U></BODY></HTML>";
	        cont="</B></TD></TR></TBODY></TABLE><BR><BR><BR><BR><BR><BR><footer> </footer></BODY></HTML>";
	     fos.write(cont.getBytes());
	     fos.close();
	     CaseCount++;
	        
	 }

	       /**
	       * takes the Screen Shot on failure and return the Screen shot file path
	       * @param className
	       * @param Result
	       * @throws Exception
	       */
	       public static String  snapShot(String fileName, String filePath) throws IOException, AWTException {
	        String osName = System.getProperty("os.name");
	        Robot rb = new Robot();
	        
	        String sTestCaseLogFolder = FPath.SCREENSHOT_FOLDER;
	        int iScreenCounter=0;
	         ArrayList<Integer> iTotalScreenCounter=new ArrayList<Integer>();
	        String commandName = "cmd.exe";
	        if (osName.equals("Windows 95")) {
	               commandName = "command.com";
	        }
	        String[] cmds = new String[9];

	        cmds[0] = commandName;
	        cmds[1] = "/C";
	        cmds[2] = FPath.SCREENSHOT_EXE;
	        cmds[3] = "/f";
	        cmds[4] = fileName;
	        cmds[5] = "/d";
	        cmds[6] = filePath;
	        cmds[7] = "/q";
	        cmds[8] = "100,7,True";
	        Process substProcess = Runtime.getRuntime().exec(cmds, null, null);
	        
	        BufferedImage screenShot = rb.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
	        iScreenCounter=iScreenCounter+1;
	        
	        ImageIO.write(screenShot, "jpg", new File(MasterScript.ScreenShotPath+"\\"+"Error.jpg"));
	        //ImageIO.write(screenShot, "jpg", new File(sTestCaseLogFolder+"\\"+iScreenCounter+".jpg"));
	        iTotalScreenCounter.add(iScreenCounter);
	        
	        return filePath+"/"+fileName;
	        
	        
	 }

	       
	       /**
	       * To convert seconds to hr:min:sec
	       * @param className
	       * @param Result
	       * @throws Exception
	       */
	       public static String getTime(long longVal){
	              //long longVal = biggy.longValue();
	           int hours = (int) longVal / 3600;
	           int remainder = (int) longVal - hours * 3600;
	           int mins = remainder / 60;
	           remainder = remainder - mins * 60;
	           int secs = remainder;
	        return hours+":"+mins+":"+secs;
	           

	    }
	       
	}



