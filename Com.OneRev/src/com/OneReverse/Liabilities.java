package com.OneReverse;

import java.awt.AWTException;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.FPath;
import com.UtilityFiles.ReportStatus;

public class Liabilities {
	static com.UtilityFiles.CommonFunctions data;
	public static final String FILEPATH_TESTDATA = FPath.TESTDATA;
	public static final String ID = "com.OneReverse.Liabilities";

	// public boolean Liabilities(Integer rowCount) throws Exception,
	// InvalidFormatException, InterruptedException, IOException {

	// public boolean Liabilities(Integer rowCount) throws
	// EncryptedDocumentException, Exception, IOException {

	ReportStatus rs = new ReportStatus();

	public ReportStatus Liabilities(Integer rowCount) throws Exception {

		rs.setReportStatus(false);

		String categoryValue = CommonFunctions.getTestData(FILEPATH_TESTDATA, "Category", ID, rowCount);
		int i = 1;
		switch (categoryValue) {
		case "Alimony":
			OneRev.Liabilities(categoryValue);
			OneRev.Alimony((CommonFunctions.getTestData(FILEPATH_TESTDATA, "Type", ID, rowCount)),
					(CommonFunctions.getTestData(FILEPATH_TESTDATA, "PaymentsOwedto", ID, rowCount)),
					(CommonFunctions.getTestData(FILEPATH_TESTDATA, "MonthlyAmount", ID, rowCount)),
					(CommonFunctions.getTestData(FILEPATH_TESTDATA, "IsMultiple", ID, rowCount)));

			break;

		case "Job Expense":
			OneRev.Liabilities(categoryValue);
			OneRev.JobExpense(CommonFunctions.getTestData(FILEPATH_TESTDATA, "Type", ID, rowCount),
					(CommonFunctions.getTestData(FILEPATH_TESTDATA, "expenses", ID, rowCount)),
					(CommonFunctions.getTestData(FILEPATH_TESTDATA, "IsMultiple", ID, rowCount)));

			break;

		}

		return rs;

	}

	public boolean nextLiabilityData(int i) throws Exception {
		String categoryValue = CommonFunctions.getTestData1(FILEPATH_TESTDATA, "Category" + i, ID);

		switch (categoryValue) {
		case "Alimony":
			OneRev.Liabilities(categoryValue);
			OneRev.Alimony((CommonFunctions.getTestData1(FILEPATH_TESTDATA, "Type" + i, ID)),
					(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "PaymentsOwedto" + i, ID)),
					(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "MonthlyAmount" + i, ID)),
					(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "IsMultiple" + i, ID)));
			/* returnVal=isNextDataPresent( i); */
			break;

		case "Job Expense":
			OneRev.Liabilities(categoryValue);
			OneRev.JobExpense(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "Type" + i, ID),
					(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "expenses" + i, ID)),
					(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "IsMultiple" + i, ID)));

			/* returnVal=isNextDataPresent( i); */
			break;

		}
		return false;
	}

	/*
	 * public boolean isNextDataPresent(int i) throws EncryptedDocumentException,
	 * InvalidFormatException, IOException { String
	 * nextDataPresent=(CommonFunctions.getTestData1(FILEPATH_TESTDATA,
	 * "IsMultiple"+i, ID)); if(nextDataPresent.equalsIgnoreCase("N")) { return
	 * false; } else { i++; return true;
	 */

}

/*
 * 
 * case "Liability" : PrimeClerk.Liabilities(categoryValue);
 * PrimeClerk.BankCreditUnion( CommonFunctions.getTestData(FILEPATH_TESTDATA,
 * "BankSourceName", ID, rowCount),
 * (CommonFunctions.getTestData(FILEPATH_TESTDATA, "NameofBank", ID, rowCount)),
 * (CommonFunctions.getTestData(FILEPATH_TESTDATA, "Accountnumber", ID,
 * rowCount)),
 * 
 * (CommonFunctions.getTestData(FILEPATH_TESTDATA, "CashorMarketValue", ID,
 * rowCount)), (CommonFunctions.getTestData(FILEPATH_TESTDATA, "type", ID,
 * rowCount)), (CommonFunctions.getTestData(FILEPATH_TESTDATA, "IsMultiple",
 * ID,rowCount)));
 * 
 * 
 * break;
 * 
 * case "Real Estate Schedule" : PrimeClerk.Liabilities(categoryValue);
 * PrimeClerk.BankCreditUnion( CommonFunctions.getTestData(FILEPATH_TESTDATA,
 * "BankSourceName", ID, rowCount),
 * (CommonFunctions.getTestData(FILEPATH_TESTDATA, "NameofBank", ID, rowCount)),
 * (CommonFunctions.getTestData(FILEPATH_TESTDATA, "Accountnumber", ID,
 * rowCount)),
 * 
 * (CommonFunctions.getTestData(FILEPATH_TESTDATA, "CashorMarketValue", ID,
 * rowCount)), (CommonFunctions.getTestData(FILEPATH_TESTDATA, "type", ID,
 * rowCount)), (CommonFunctions.getTestData(FILEPATH_TESTDATA, "IsMultiple",
 * ID,rowCount)));
 * 
 * 
 */