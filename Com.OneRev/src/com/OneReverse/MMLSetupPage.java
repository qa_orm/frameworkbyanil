package com.OneReverse;



import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.UtilityFiles.GenericLibrary;

public class MMLSetupPage {
		
	public static By btnMMLSetup  = By.xpath("//*[@id='MML_Setup']");
	public static By btnAddCustom = By.xpath("//*[@id='CustomMMlType']");
	public static By txtMMLType   = By.id("txtVal");
	public static By btnSubmit    = By.xpath("//*[@title='Submit']");
	public static By tableMMLType = By.xpath("//*[@id='gview_jqMMLTable']//tr//td[1]");
	
	
	public static ArrayList<Boolean> statusList=new ArrayList<Boolean>();
	static Boolean Sts = false;
	static String StepSts;
	public static String getMMLType;
	
	public static void popupAddMMLType(String mmlType){
		
		OneRev.driver.findElement(btnMMLSetup).click();
		OneRev.driver.findElement(btnAddCustom).click();
		OneRev.driver.switchTo().frame("ifrmcontent");
		
		getMMLType = mmlType+OneRev.uniqueNum;
		OneRev.driver.findElement(txtMMLType).sendKeys(mmlType+OneRev.uniqueNum);
		OneRev.driver.switchTo().defaultContent();
		OneRev.driver.findElement(btnSubmit).click();
		
	}
	
	
	public static void verifyMMLTableElement(String mmlelement) throws Exception
	{
		Thread.sleep(5000L);
		OneRev.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		List<WebElement> tableElement= OneRev.driver.findElements(tableMMLType);
		
		System.out.println(tableElement.size());
				
		for(int i=0;i<tableElement.size();i++)
		{
			//Thread.sleep(2000L);
			if(mmlelement.equals(tableElement.get(i).getText())){
				
												
				String listOfElements = tableElement.get(i).getText();
				Thread.sleep(2000L);
				System.out.println(getMMLType+"<==================================>"+listOfElements);
				
				if(StepSts!=null){
				
				GenericLibrary.setCellData("The MML Type  "  +getMMLType+ " ","The MML Type "  +getMMLType+" should present in the MML Type table",
    					"The MML Type "  +getMMLType+" is NOT present in the MML Type table","Fail");
					statusList.add(false);
					Sts=false;
				} else{
				GenericLibrary.setCellData("The MML Type  "  +getMMLType+ " ","The MML Type "  +getMMLType+" should present in the MML Type table",
						   "The MML Type "  +getMMLType+" is present in the MML Type table","Pass");
				statusList.add(true);
				Sts=true;
				
				}
				
							
												
				break;
			} 
			
			//System.out.println("Element NOT Found");
			
		}
		
	}

}
