package com.OneReverse;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.FPath;
public class LoanDetails {
	static com.UtilityFiles.CommonFunctions data;
	public static final String FILEPATH_TESTDATA = FPath.TESTDATA;
	public static final String ID = "com.OneReverse.LoanDetails";

	public  boolean LoanDetails(Integer rowCount) throws EncryptedDocumentException, Exception, IOException {
		//String categoryValue=CommonFunctions.getTestData(FILEPATH_TESTDATA, "Category", ID);
		
		OneRev.LoanDetails(CommonFunctions.getTestData(FILEPATH_TESTDATA, "ClosingAgentEscrowOfficer", ID, rowCount),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "TitleCompany", ID, rowCount)),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "ContactName", ID, rowCount)),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "Contactph", ID, rowCount)),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "HastheClientpaidFee", ID, rowCount)),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "Story", ID, rowCount)));
		
		
		
		return false;
		
	}
}

