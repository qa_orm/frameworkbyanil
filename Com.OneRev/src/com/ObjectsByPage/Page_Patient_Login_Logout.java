package com.ObjectsByPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Page_Patient_Login_Logout {
	
	public Page_Patient_Login_Logout(WebDriver drv) {
		PageFactory.initElements(drv, this);
	}
	
	
	//User ID text box element 
	@FindBy(xpath="//input[@id='accountid']")
	private WebElement userID;
	
	public WebElement UserID()
	{
		return userID;
	}
	
	//Password text box element 
	@FindBy(xpath="//input[@id='password']")
	private WebElement pwd;
	
	public WebElement Password()
	{
		return pwd;
	}
	
	//Login Button element 
		@FindBy(xpath="//button[@id='loginBtn']")
		private WebElement LoginBtn;
		
		public WebElement LoginButton()
		{
			return LoginBtn;
		}
	
	//LogOut Element  
				@FindBy(xpath="//div[@id='userOpt']/ul/li[6]/a")
				private WebElement LogOutbtn;
				
				public WebElement LogOutbtn()
				{
					return LogOutbtn;
				}

}
