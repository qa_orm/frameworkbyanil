package com.OneReverse;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.FPath;
public class Employment {
	static com.UtilityFiles.CommonFunctions data;
	public static final String FILEPATH_TESTDATA = FPath.TESTDATA;
	public static final String ID = "com.OneReverse.Employment";

	public  boolean Employment(Integer rowCount) throws EncryptedDocumentException, Exception, IOException {
		
		
		OneRev.Employment(CommonFunctions.getTestData(FILEPATH_TESTDATA, "selfemployed", ID, rowCount),
				CommonFunctions.getTestData(FILEPATH_TESTDATA, "IncomeType", ID, rowCount),
						CommonFunctions.getTestData(FILEPATH_TESTDATA, "EmployerName", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "PosTitle", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "EmployerPhone", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "EmployerAddress", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "City", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "State", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "Zip", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "Years", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "Months", ID, rowCount),
		CommonFunctions.getTestData(FILEPATH_TESTDATA, "YearsEmployed", ID, rowCount));
		
				
				
				return false;
		
		
		
	}
}
	
	
	