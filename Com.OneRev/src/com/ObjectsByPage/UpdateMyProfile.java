package com.ObjectsByPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class UpdateMyProfile {
	public UpdateMyProfile(WebDriver drv) {
		PageFactory.initElements(drv, this);
	}
//UpdateMyProfile link element 	
	@FindBy(linkText="Update My Profile")
	private WebElement profile;
	
	public WebElement UpdateProfile()
	{
		return profile;
	}

//Phone link element 
	@FindBy(xpath=".//*[@id='dashboardUpdateProfile']/ul/li[2]/a")
	private WebElement Phone;
		
	public WebElement Phone()
	{
		return Phone;
	}	

//Phone Num Text Field  element 	
	@FindBy(xpath="//input[@id='PHONE_NUMBER']")
	private WebElement PhoneNum;
			
	public WebElement Phone_Num()
	{
		return PhoneNum;
	}	
	
//Submit Button in Phone element 	
	@FindBy(xpath="//button[@id='phoneSubmitButton']")
	private WebElement Submit;
				
	public WebElement Submitbtn()
	{
		return Submit;
	}
	
//Success msg element 	
		@FindBy(xpath="//span[@id='phoneUpdateStatus']")
		private WebElement Success;
					
		public WebElement Success_msg()
		{
			return Success;
		}
}
