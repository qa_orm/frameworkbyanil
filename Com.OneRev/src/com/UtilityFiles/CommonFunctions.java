package com.UtilityFiles;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.DriverScript.MasterScript;

public class CommonFunctions {

	public static String[] getTestCaseId(String filePath, int rowCount)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		String testCaseId = null;
		FileInputStream fileInputStream = new FileInputStream(filePath);
		Workbook wb = WorkbookFactory.create(fileInputStream);
		org.apache.poi.ss.usermodel.Sheet s = wb.getSheet(MasterScript.sheetName);

		Row row = s.getRow(0);
		int lastrow = s.getLastRowNum();// Integer.parseInt(s.getLastRowNum()//.toString());
		String testId[] = new String[1];

		System.out.println(lastrow + " lastrow ");
		String idValue = null;
		int rowindex = -1;
		// for (int j = rowCount; j ==rowCount+1; j++) {
		testCaseId = s.getRow(rowCount).getCell(0).toString();
		testId[0] = testCaseId;
		lastrow--;
		// }
		return testId;

	}

	public static String[] getTestCaseId1(String filePath, int rowCount)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		String ar[] = new String[1500];
		int x = 0;
		FileInputStream fileInputStream = new FileInputStream(filePath);
		Workbook wb = WorkbookFactory.create(fileInputStream);
		org.apache.poi.ss.usermodel.Sheet s = wb.getSheet(MasterScript.sheetName);
		Row row = s.getRow(0);
		// String []testId=new String[s.getLastRowNum()];

		int lastrow = s.getLastRowNum();// Integer.parseInt(s.getLastRowNum()//.toString());
		String testId[] = new String[lastrow];
		System.out.println(lastrow + " lastrow ");
		String idValue = null;
		int rowindex = -1;
		int colIndex = 2;
		// for (int j = rowCount; j == rowCount+1; j++) {

		if (s.getRow(rowCount).getCell(colIndex) != null) {
			ar[x] = s.getRow(rowCount).getCell(colIndex).getStringCellValue();

			colIndex = colIndex + 1;
			x = x + 1;

		}
		// }
		return ar;

	}

	public static String screenshot(WebDriver drv, String ScreName) {
		try {
			TakesScreenshot ts = (TakesScreenshot) drv;
			File src = ts.getScreenshotAs(OutputType.FILE);
			String dest = "D:\\FrameWork\\Snap\\" + ScreName + ".png";
			File destination = new File(dest);
			FileUtils.copyFile(src, destination);
			System.out.println("screen captured");
			return dest;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return e.getMessage();
		}
	}

	public static String getTestData(String filePath, String Name, String Id, int rowCount)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		FileInputStream fileInputStream = new FileInputStream(filePath);
		String namOfButton = null;
		Workbook wb = WorkbookFactory.create(fileInputStream);
		org.apache.poi.ss.usermodel.Sheet s = wb.getSheet(MasterScript.sheetName);
		org.apache.poi.ss.usermodel.Row row = s.getRow(0);
		int lastrow = s.getLastRowNum();// Integer.parseInt(s.getLastRowNum()//.toString());
		System.out.println(lastrow + " lastrow ");
		String idValue = null;
		int rowindex = -1;
		for (int j = rowCount; j <= lastrow; j++) {

			System.out.println(Id + " === " + s.getRow(j).getCell(0).toString());

			if (Id.equals(s.getRow(j).getCell(0).toString())) {

				idValue = s.getRow(j).getCell(0).toString();
				rowindex = j;
				break;

			}

		}

		int lastCell = row.getPhysicalNumberOfCells();
		int cellNumber = -1;
		System.out.println(lastCell + " number...");
		for (int i = 0; i <= lastCell; i++) {
			if (row.getCell(i) != null) {
				String name = row.getCell(i).toString();
				System.out.println(Name == name);
				System.out.println(Name.equals(name) + " equal");
				System.out.println(Name + " === " + name);
				if (Name.trim().equals(name.trim())) {
					System.out.println(i + "  : " + row.getCell(i).toString());
					cellNumber = i;
					break;
				}
				System.out.println(i + " End .... for loop =" + row.getCell(i).toString());
				System.out.println(Name.equals(row.getCell(i).toString()) + "boolean");
			}
		}
		if (rowindex != -1 && cellNumber != -1) {
			System.out.println(cellNumber + "  :  " + idValue);
			System.out.println(rowindex + " numbers  " + cellNumber);

			namOfButton = s.getRow(rowindex).getCell(cellNumber).toString();
			System.out.println(namOfButton + "   namOfButton");

		}
		return namOfButton;

	}

	public static String getTestData1(String filePath, String Name, String Id)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		FileInputStream fileInputStream = new FileInputStream(filePath);
		String namOfButton = null;
		// Workbook wb = WorkbookFactory.create(inp)

		Workbook wb = WorkbookFactory.create(fileInputStream);
		org.apache.poi.ss.usermodel.Sheet s = wb.getSheet(MasterScript.sheetName);
		org.apache.poi.ss.usermodel.Row row = s.getRow(0);
		int lastrow = s.getLastRowNum();// Integer.parseInt(s.getLastRowNum()//.toString());
		System.out.println(lastrow + " lastrow ");
		String idValue = null;
		int rowindex = -1;
		for (int j = 0; j <= lastrow; j++) {

			System.out.println(Id + " === " + s.getRow(j).getCell(0).toString());

			if (Id.equals(s.getRow(j).getCell(0).toString())) {

				idValue = s.getRow(j).getCell(0).toString();
				rowindex = j;
				break;
			}

		}

		int lastCell = row.getPhysicalNumberOfCells();
		int cellNumber = -1;
		System.out.println(lastCell + " number...");
		for (int i = 0; i <= lastCell; i++) {
			if (row.getCell(i) != null) {
				String name = row.getCell(i).toString();
				System.out.println(Name == name);
				System.out.println(Name.equals(name) + " equal");
				System.out.println(Name + " === " + name);
				if (Name.equals(name)) {
					System.out.println(i + "  : " + row.getCell(i).toString());
					cellNumber = i;
					break;
				}
				System.out.println(i + " End .... for loop =" + row.getCell(i).toString());
				System.out.println(Name.equals(row.getCell(i).toString()) + "boolean");
			}
		}

		if (rowindex != -1 && cellNumber != -1) {
			System.out.println(cellNumber + "  :  " + idValue);
			System.out.println(rowindex + " numbers  " + cellNumber);

			namOfButton = s.getRow(rowindex).getCell(cellNumber).toString();
			System.out.println(namOfButton + "   namOfButton");
		}
		return namOfButton;

	}

}
