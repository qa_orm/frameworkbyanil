package com.UtilityFiles;

public class ReportStatus {
	private boolean reportStatus=true;
	private boolean nextRowPresent=false;
	public boolean isReportStatus() {
		return reportStatus;
	}
	public void setReportStatus(boolean reportStatus) {
		this.reportStatus = reportStatus;
	}
	public boolean isNextRowPresent() {
		return nextRowPresent;
	}
	public void setNextRowPresent(boolean nextRowPresent) {
		this.nextRowPresent = nextRowPresent;
	}
	
}
