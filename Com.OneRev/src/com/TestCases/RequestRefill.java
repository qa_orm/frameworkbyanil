package com.TestCases;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Specific_Methods.TestCase_Specific_Methods;
import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.CommonUtils;
import com.UtilityFiles.Constants;
import com.UtilityFiles.FPath;

public class RequestRefill {

	FPath FP;
	CommonFunctions common;
	CommonUtils com;
	TestCase_Specific_Methods spm;
	WebDriver dr;
	Constants con;
	CommonUtils com1 = new CommonUtils();
	public ArrayList<Boolean> statusList1=new ArrayList<Boolean>();
//	public static final String ID="com.TestCases.RequestRefill";
	
	
	public boolean RequestRefill() throws InterruptedException{
		preCond();
		Refill();
		postCond();
		statusList1.addAll(spm.statusList);
		return statusList1.contains(false);
	}
	
	  public boolean preCond() throws InterruptedException
	  {
		
		try {
			com1.getDriverObject();
			FP=new FPath();
			common=new CommonFunctions();
			spm=new TestCase_Specific_Methods();
			//dr=com1.getDriverObject();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	  }
	
	public boolean Refill()
	{
		boolean tc_res = false;
		try {
			spm.SelectProfile(com1.drv);
			spm.LoginBriovaRx(com1.drv, Constants.ID, Constants.PWD);
			spm.RefillablePrescription(com1.drv);
			spm.RxNum(com1.drv);
			spm.IUnderstand(com1.drv);
			spm.FirstName(com1.drv);
			spm.LastName(com1.drv);
			spm.DateofBirth(com1.drv);
			spm.Calander(com1.drv);
			spm.Selectdate(com1.drv);
			spm.supply_medication(com1.drv);
			spm.medication_changes(com1.drv);
			spm.latexAllergies(com1.drv);
			spm.pharmacist(com1.drv);
			spm.NurseAdminister(com1.drv);
			spm.additional_comments(com1.drv);
			spm.CreditCardInfo(com1.drv);
			spm.ExpiryMonth(com1.drv);
			spm.ExpiryYear(com1.drv);
			spm.MaxAmount(com1.drv);
			spm.Acceptence(com1.drv);
			spm.Continue_review(com1.drv);
			spm.Submit_request(com1.drv);
			spm.VerifyStatus(com1.drv);
			
		} catch (Exception e) {
			Reporter.log("Error " + e.getMessage()); 	
			tc_res = false;
		}
		return tc_res;
	}
	
	  public boolean postCond()
	  {
		try {
			com1.drv.close();
			com1.drv.quit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	  }
	
}
