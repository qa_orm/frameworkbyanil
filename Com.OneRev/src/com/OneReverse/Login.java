package com.OneReverse;



import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import com.UtilityFiles.FPath;
import com.UtilityFiles.ReportStatus;

public class Login{

	static com.UtilityFiles.CommonFunctions data;
	public static final String FILEPATH_TESTDATA = FPath.TESTDATA;
	public static final String ID = "com.OneReverse.Login";
	ReportStatus rs=new ReportStatus();
	
	public  ReportStatus Login(Integer rowCount) throws Exception {
		// Load the selected Browser
		rs.setReportStatus(false);
		OneRev.loadUrl("Chrome");

		// Enter the login credentials from xl sheet.
		OneRev.login(data.getTestData(FILEPATH_TESTDATA, "UserName", ID, rowCount),
				data.getTestData(FILEPATH_TESTDATA, "Password", ID, rowCount));
		
	
		//PrimeClerk.appdate1(data.getTestData1(FILEPATH_TESTDATA, "appdate", ID));
		
		//PrimeClerk.loan();
		//PrimeClerk.loan("");
				
		
		//PrimeClerk.loan("");
		
		//PrimeClerk.loan("");
		return rs;
		
	//	PrimeClerk.appdate

		// Enter the appdate
		
		
	}

	

	

}


	//	PrimeClerk.loan(data.getTestData(FILEPATH_TESTDATA, "MortgageAppliedfor", ID));
		
		//PrimeClerk.pol(data.getTestData(FILEPATH_TESTDATA, "PurposeofLoan", ID));
		
		//PrimeClerk.LPP(data.getTestData(FILEPATH_TESTDATA, "LoanPaymentPlan", ID));
		
	//	PrimeClerk.EAV(data.getTestData(FILEPATH_TESTDATA, "EAV", ID));
	//	PrimeClerk.lof(data.getTestData(FILEPATH_TESTDATA, "EAV", ID));
	//	PrimeClerk.atB(data.getTestData(FILEPATH_TESTDATA, "EAV", ID));
	//	PrimeClerk.lofn(data.getTestData(FILEPATH_TESTDATA, "EAV", ID));

		/*

		WebElement sel2 = PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[3]/div/select"));

		Select sl2 = new Select(sel2);

		sl2.selectByIndex(3);

		Thread.sleep(5000L);

		PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[4]/div/input"))
				.sendKeys("123456789");

		Thread.sleep(3000L);
		WebElement w = PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[5]/div/select"));

		Select sl = new Select(w);

		sl.selectByIndex(2);

		Thread.sleep(1000L);

		WebElement atb = PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[7]/div/select"));

		Select sl3 = new Select(atb);

		sl3.selectByIndex(02);

		Thread.sleep(1000L);

		PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[8]/div/input"))
				.sendKeys("Gopinath");

		Thread.sleep(1000L);

		PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[9]/div/input"))
				.sendKeys("Ramadoss");

		Thread.sleep(1000L);

		PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[10]/div/div/input"))
				.sendKeys("09/13/2017");

		Thread.sleep(1000L);

		PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[11]/div/input"))
				.sendKeys("1234567890");

		PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[12]/div/input"))
				.click();
		Thread.sleep(1000L);
		PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[12]/div/input"))
				.sendKeys("One Reverse Mortgage LLC");

		PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[13]/div/input"))
				.sendKeys("#204, Detroit automation team");
		PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[14]/div/input"))
				.sendKeys("Bangalore");
		Thread.sleep(500L);

		WebElement state = PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[15]/div/select"));

		Select sl4 = new Select(state);

		sl4.selectByIndex(05);

		PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[16]/div/input"))
				.sendKeys("23456");

		PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[17]/div/input"))
				.sendKeys("Loan identifier 123");
		PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[18]/div/input"))
				.sendKeys("Company Identifier 456");

		WebElement hhold = PrimeClerk.driver.findElement(By.xpath(
				"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[19]/div/select"));

		Select sl5 = new Select(hhold);

		sl5.selectByIndex(02);

		Thread.sleep(2000L);

		System.out.println("Before Age");

	//try {
		WebElement age = PrimeClerk.driver.findElement(By.xpath(
			"html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[20]/div/select"));

		Select sl9 = new Select(age);

		sl9.selectByValue("Yes");


		Thread.sleep(1000L);
		
		//PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Is there a POA?')]/../following-sibling::select")).click();
		WebElement poa = PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Is there a POA?')]/../following-sibling::select"));
	    Select sl7 = new Select(poa);
	    sl7.selectByValue("Yes");
	//sl7.selectByIndex(1);

	System.out.println("After POA");


		
		PrimeClerk.driver.findElement(By.xpath(
				"//span[contains(text(),'If yes, Name of POA')]/../following-sibling::input"))
			.sendKeys("POA IS Yes");

		Thread.sleep(200L);

		PrimeClerk.driver.findElement(By.xpath(
				"//span[contains(text(),'Square Footage ')]/../following-sibling::input"))
				.sendKeys("600");

		Thread.sleep(1000L);

		PrimeClerk.driver.findElement(By.xpath(
				"//span[contains(text(),'Alternate Contact Name ')]/../following-sibling::input"))
				.sendKeys("Alternate Contact NAme");

		Thread.sleep(1000L);

		PrimeClerk.driver.findElement(By.xpath(
				"//span[contains(text(),'Alternate Contact Full Address ')]/../following-sibling::input"))
				.sendKeys("Alternate Contact full Address");

		Thread.sleep(500L);

		PrimeClerk.driver.findElement(By.xpath(
				"//span[contains(text(),'Alternate Contact Phone Number ')]/../following-sibling::input"))
				.sendKeys("1234567890");

		Thread.sleep(500L);
		
		PrimeClerk.driver.findElement(By.xpath(
				"//span[contains(text(),'Alternate Contact Full Address ')]/../following-sibling::input")).click();

		PrimeClerk.driver.findElement(By.xpath(
				"//button[contains(.,'Save')]")).click();

	WebElement loanid = PrimeClerk.driver.findElement(By.xpath("//h3[@id='lblLoanID']"));
		
		//System.out.println(loanid.
		
		

//------------------- Subject Proprerty---------------------------------------//
		
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Subject Property Address *')]/../following-sibling::input")).sendKeys("testing Junit Framework");
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Subject Property City *')]/../following-sibling::input")).sendKeys("Test city");
	
	Thread.sleep(500L);
	
	PrimeClerk.driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[3]/div/div/fieldset/div/div/div[5]/div/input")).sendKeys("23456");
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Legal Description of Property')]/../following-sibling::input")).sendKeys("23456");
	WebElement rtype=PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Residence Type *')]/../following-sibling::select"));
	
	Select sl11 = new Select(rtype);
	
	sl11.selectByIndex(1);
	
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'No of Units *')]/../following-sibling::input")).sendKeys("5");
	
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Year Built *')]/../following-sibling::input")).sendKeys("1990");
	
WebElement held=PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Property Held as *')]/../following-sibling::select"));
	
	Select sl12 = new Select(held);
	
	sl12.selectByIndex(1);
	
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Property Title is Held in These Names')]/../following-sibling::input")).sendKeys("Testing Selenium");
	
	
	
WebElement title=PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Check If Title is also Held As')]/../following-sibling::select"));
	
	Select sl13 = new Select(title);
	
	sl13.selectByIndex(1);
	
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Real Estate Taxes - Monthly *')]/../following-sibling::input")).sendKeys("1990");
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Hazard Insurance - Monthly *')]/../following-sibling::input")).sendKeys("1990");
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Miscellaneous (HOA, PUD, Condo Fees, Other Insurances) - Monthly')]/../following-sibling::input")).sendKeys("1990");
	PrimeClerk.driver.findElement(By.xpath(
			"//button[contains(.,'Save')]")).click();
	
	Thread.sleep(500L);
	
	

		//PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Subject Property Address *')]/../following-sibling::input"")).sendKeys("Subject P address");"
		
		//PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Subject Property Address *')]/../following-sibling::input"")).sendKeys("Subject P address"));"
			//	+ "
		//PrimeClerk.Subject_property();
		
		//public static void Subject_property();
		 
//----------------------------------Client------------------------------------------------------//
	Thread.sleep(1000L);
	
	PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Add')]")).click();
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'First Name')]/../following-sibling::input")).sendKeys("Gopinath");
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Middle Name')]/../following-sibling::input")).sendKeys("Middle Name");
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Last Name')]/../following-sibling::input")).sendKeys("Last Name");
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Current Address')]/../following-sibling::input")).sendKeys("Galaxe Solutions Detroit");
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Current City')]/../following-sibling::input")).sendKeys("Detroit");
WebElement cstate =	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Current State')]/../following-sibling::select"));

Select sl14 = new Select(cstate);

sl14.selectByIndex(5);

PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Current Zip')]/../following-sibling::input")).sendKeys("23456");

PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Number of Years in Current Address ')]/../following-sibling::input")).sendKeys("10");
PrimeClerk.driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[4]/fieldset[2]/div[1]/div/div[2]/div/div/fieldset[2]/div/div[3]/div[1]/input")).click();
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Client Email Address ')]/../following-sibling::input")).sendKeys("Somemail@mail.com");
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Home Phone ')]/../following-sibling::input")).sendKeys("1234567890");
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Mobile Phone')]/../following-sibling::input")).sendKeys("9876543210");
PrimeClerk.driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[4]/fieldset[2]/div[1]/div/div[2]/div/div/fieldset[2]/div/div[3]/div[5]/div/div/input")).sendKeys("08/19/1950");
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Social Security Number ')]/../following-sibling::input")).sendKeys("123456789");
WebElement dependents = PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Number of Dependants')]/../following-sibling::select"));

Select sl15= new Select(dependents);

sl15.selectByIndex(5);

WebElement yos = PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Years of School')]/../following-sibling::select"));

Select sl16= new Select(yos);

sl16.selectByIndex(5);

WebElement ms = PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Marital Status')]/../following-sibling::select"));

Select sl17= new Select(ms);

sl17.selectByIndex(2);

WebElement crtype = PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Residence')]/../following-sibling::select"));

Select sl18= new Select(crtype);

sl18.selectByIndex(2);

WebElement ptype = PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Property type')]/../following-sibling::select"));

Select sl19= new Select(ptype);

sl19.selectByIndex(3);

PrimeClerk.driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[4]/fieldset[2]/div[1]/div/div[2]/div/div/fieldset[2]/div/div[3]/div[12]/input")).click();
Thread.sleep(1000L);

WebElement nbspouse = PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Non-Borrowing Spouse *')]/../following-sibling::select"));

Select sl20= new Select(nbspouse);

sl20.selectByValue("No");

PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Save')]")).click();

//---------------------Employment---------------------------


Thread.sleep(5000L);



//PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Next')]")).click();
PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Next')]")).click();
System.out.println("After Next");

//List<WebElement> list = PrimeClerk.driver.findElements(By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[5]/div/fieldset/div/fieldset/div/div[1]/div[1]/div/div/select"));

//Select oSelect = new Select(PrimeClerk.driver.findElement(By.name("selClient")));
		
		//xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[5]/div/fieldset/div/fieldset/div/div[1]/div[1]/div/div/select")));
//List <WebElement> elementCount = oSelect.getOptions();
//int iSize = elementCount.size();

//for(int i =0; i<iSize ; i++){
	//String sValue = elementCount.get(i).getText();
	//oSelect.selectByIndex(2);
//	}

//WebElement client = PrimeClerk.driver.findElement(By.xpath("//*[@class='slds-select_container']"));
Thread.sleep(5000L);
Select dropDown = new Select(PrimeClerk.driver.findElement(By.xpath("//select[@name=\"selClient\"]")));
//Select dropDown = new Select(PrimeClerk.driver.findElement(By.name("selClient")));
//PrimeClerk.driver.findElement(By.name("selClient"));
dropDown.selectByIndex(1);


//Select sl21 = new Select(client);

//sl21.selectByValue("LastName, Gopinath");


PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Add')]")).click();

//--- Employment-------------------------------------

//Select empld = PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Self Employed ')]/../following-sibling::select")));


Select dropDown1 = new Select(PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Self Employed ')]/../following-sibling::select")));
//Select dropDown = new Select(PrimeClerk.driver.findElement(By.name("selClient")));
//PrimeClerk.driver.findElement(By.name("selClient"));
dropDown1.selectByValue("Yes");

Select dropDown2 = new Select(PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Current Position (Optional)')]/../following-sibling::select")));
//Select dropDown = new Select(PrimeClerk.driver.findElement(By.name("selClient")));
//PrimeClerk.driver.findElement(By.name("selClient"));
dropDown2.selectByValue("Yes");

Select dropDown3 = new Select(PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'IncomeType ')]/../following-sibling::select")));
//Select dropDown = new Select(PrimeClerk.driver.findElement(By.name("selClient")));
//PrimeClerk.driver.findElement(By.name("selClient"));
dropDown3.selectByIndex(2);

PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Employer Name')]/../following-sibling::input")).sendKeys("Google Inc");

PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Position/Title')]/../following-sibling::input")).sendKeys("Senior QA Engineer");

PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Employer Phone')]/../following-sibling::input")).sendKeys("9639639630");

PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Employer Address')]/../following-sibling::input")).sendKeys("1600 Amphitheatre Parkway Mountain View");

PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'City')]/../following-sibling::input")).sendKeys("California");


Select dropDown4 = new Select(PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'State')]/../following-sibling::select")));
//Select dropDown = new Select(PrimeClerk.driver.findElement(By.name("selClient")));
//PrimeClerk.driver.findElement(By.name("selClient"));
dropDown4.selectByIndex(5);
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Zip')]/../following-sibling::input")).sendKeys("23456");
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Year ')]/../following-sibling::input")).sendKeys("10");
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Month ')]/../following-sibling::input")).sendKeys("02");
PrimeClerk.driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[5]/div/fieldset/div[2]/div/div[2]/div/fieldset/div/div/div[5]/div[3]/div/input")).sendKeys("09/19/2001");
PrimeClerk.driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[5]/div/fieldset/div[2]/div/div[2]/div/fieldset/div/div/div[5]/div[4]/div/input")).sendKeys("09/19/2001");
PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Save')]")).click();

//-------------------------Income-------------------
PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Next')]")).click();


Thread.sleep(5000L);
Select dropDown7 = new Select(PrimeClerk.driver.findElement(By.xpath("//select[@name=\"selClient\"]")));
//Select dropDown = new Select(PrimeClerk.driver.findElement(By.name("selClient")));
//PrimeClerk.driver.findElement(By.name("selClient"));
dropDown7.selectByIndex(1);

//Select dropDown5 = new Select(PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'IncomeType ')]/../following-sibling::select")));
//Select dropDown = new Select(PrimeClerk.driver.findElement(By.name("selClient")));
//PrimeClerk.driver.findElement(By.name("selClient"));
//dropDown5.selectByIndex(2);

Thread.sleep(1000L);

PrimeClerk.driver.findElement(By.xpath("//a[@name='divSocialSecurity']")).click();
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Social Security Income')]/../following-sibling::input")).clear();

Thread.sleep(1500L);
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Social Security Income')]/../following-sibling::input")).sendKeys("23456");

PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Save')]")).click();
Thread.sleep(4000L);

PrimeClerk.driver.findElement(By.xpath("//a[@name='divPensionRetirement']")).click();
Thread.sleep(1500L);
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Pension Retirement')]/../following-sibling::input")).clear();
Thread.sleep(1500L);
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Pension Retirement')]/../following-sibling::input")).sendKeys("698");
Thread.sleep(4000L);
PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Save')]")).click();
Thread.sleep(1500L);
PrimeClerk.driver.findElement(By.xpath("//a[@name='divDividend']")).click();
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Dividend Income')]/../following-sibling::input")).clear();
Thread.sleep(1500L);
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Dividend Income')]/../following-sibling::input")).sendKeys("852");
PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Save')]")).click();
Thread.sleep(4000L);
PrimeClerk.driver.findElement(By.xpath("//a[@name='divBase']")).click();
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Base Employment Income')]/../following-sibling::input")).clear();
Thread.sleep(1500L);
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Base Employment Income')]/../following-sibling::input")).sendKeys("987");
PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Save')]")).click();

Thread.sleep(4000L);
PrimeClerk.driver.findElement(By.xpath("//a[@name='divNetRental']")).click();
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Net Rental Income')]/../following-sibling::input")).clear();
Thread.sleep(1500L);
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Net Rental Income')]/../following-sibling::input")).sendKeys("23456");
PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Save')]")).click();
Thread.sleep(1500L);


///------------------Assets


PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Next')]")).click();
Thread.sleep(1500L);

Select dropDown8 = new Select(PrimeClerk.driver.findElement(By.xpath("//select[@name='pickClient']")));
//Select dropDown = new Select(PrimeClerk.driver.findElement(By.name("selClient")));
//PrimeClerk.driver.findElement(By.name("selClient"));
dropDown8.selectByIndex(1);

Thread.sleep(2000L);

WebElement category = PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Category')]/../following-sibling::select"));

Select sl21= new Select(category);

//sl21.selectByValue("No");

sl21.selectByIndex(2);

//Select category = new Select(PrimeClerk.driver(By.xpath("//span[contains(text(),'Category')]/../following-sibling::select")));

//category.selectByIndex(1);

PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Add')]")).click();

Thread.sleep(1000L);

PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Company Name/Number *')]/../following-sibling::input")).sendKeys("987654321");
Thread.sleep(1000L);
PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Cash or Market Value *')]/../following-sibling::input")).sendKeys("7500");
Thread.sleep(1000L);


WebElement type = PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Type *')]/../following-sibling::select"));
Select sl22= new Select(type);

//sl21.selectByValue("No");

sl22.selectByIndex(2);

PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Save')]")).click();


PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Next')]")).click();


//----------------------------- lIabilities----------------------------------------------------
Thread.sleep(1000L);

Select dropDown9 = new Select(PrimeClerk.driver.findElement(By.xpath("//select[@name='selclient']")));
//Select dropDown = new Select(PrimeClerk.driver.findElement(By.name("selClient")));
//PrimeClerk.driver.findElement(By.name("selClient"));
dropDown9.selectByIndex(1);

Thread.sleep(7000L);


Select dropDown10 = new Select(PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Category')]/../following-sibling::select")));
dropDown10.selectByIndex(1);

Thread.sleep(500L);


Select dropDown11 = new Select(PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Type')]/../following-sibling::select")));
dropDown11.selectByIndex(1);

Thread.sleep(500L);

PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Add')]")).click();

Thread.sleep(500L);

PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Payments Owed to')]/../following-sibling::input")).sendKeys("Julie");

Thread.sleep(1000L);

PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Amount *')]/../following-sibling::input")).sendKeys("7500");
Thread.sleep(500L);

PrimeClerk.driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[8]/div[3]/div/div[2]/div/div[6]/input")).click();

Thread.sleep(5000L);
PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Save')]")).click();
Thread.sleep(5000L);
PrimeClerk.driver.findElement(By.xpath("//button[contains(.,'Next')]")).click();

Thread.sleep(1000L);

Select dropDown25 = new Select(PrimeClerk.driver.findElement(By.xpath("//select[@name='selPickClient']")));
//Select dropDown = new Select(PrimeClerk.driver.findElement(By.name("selClient")));
//PrimeClerk.driver.findElement(By.name("selClient"));
dropDown25.selectByIndex(1);

Thread.sleep(2000L);


return false;
		
	}

}
*/
