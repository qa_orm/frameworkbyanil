
/*
package com.primeclerk;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.FPath;
public class Assets {
	static com.UtilityFiles.CommonFunctions data;
	public static final String FILEPATH_TESTDATA = FPath.TESTDATA;
	public static final String ID = "com.primeclerk.Assets";

	public  boolean Assets() throws EncryptedDocumentException, Exception, IOException {
	//	String categoryValue=CommonFunctions.getTestData(FILEPATH_TESTDATA, "Category", ID);
		
		
		
		PrimeClerk.Assets(CommonFunctions.getTestData(FILEPATH_TESTDATA, "Category", ID),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "AssetSourceName", ID)),
						(CommonFunctions.getTestData(FILEPATH_TESTDATA, "FaceAmount", ID)));
				
	
				
				return false;
	
	}
				
				
	}
	
	
	*/



package com.OneReverse;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.FPath;
import com.UtilityFiles.ReportStatus;
public class Assets {
	static com.UtilityFiles.CommonFunctions data;
	public static final String FILEPATH_TESTDATA = FPath.TESTDATA;
	public static final String ID = "com.OneReverse.Assets";
	
	//public  boolean Assets(Integer rowCount) throws EncryptedDocumentException, Exception, IOException {
	ReportStatus rs=new ReportStatus();
	public ReportStatus Assets(Integer rowCount) throws Exception {
		
		rs.setReportStatus(false);	
	
	
	
	String categoryValue=CommonFunctions.getTestData(FILEPATH_TESTDATA, "Category", ID, rowCount);
		int i =1;
		switch (categoryValue) {	
		case "Life Insurance"	:	
			OneRev.Assets(categoryValue);
			OneRev.lifeInsurance(
					(CommonFunctions.getTestData(FILEPATH_TESTDATA, "AssetSourceName", ID, rowCount)),
							(CommonFunctions.getTestData(FILEPATH_TESTDATA, "FaceAmount", ID, rowCount)),
							(CommonFunctions.getTestData(FILEPATH_TESTDATA, "IsMultiple", ID,rowCount)));		
					
					break;
					
					
					
		case "Bank/Credit Union" :
			OneRev.Assets(categoryValue);
			OneRev.BankCreditUnion(
					CommonFunctions.getTestData(FILEPATH_TESTDATA, "BankSourceName", ID, rowCount),
					(CommonFunctions.getTestData(FILEPATH_TESTDATA, "NameofBank", ID, rowCount)),
							(CommonFunctions.getTestData(FILEPATH_TESTDATA, "Accountnumber", ID, rowCount)),
							
							(CommonFunctions.getTestData(FILEPATH_TESTDATA, "CashorMarketValue", ID, rowCount)),
							(CommonFunctions.getTestData(FILEPATH_TESTDATA, "type", ID, rowCount)),
							(CommonFunctions.getTestData(FILEPATH_TESTDATA, "IsMultiple", ID,rowCount)));
			

			
			break;
			
			
			
		case "Stock&Bonds" :
			OneRev.Assets(categoryValue);
			
			OneRev.StockBonds(
					CommonFunctions.getTestData(FILEPATH_TESTDATA, "StockSourceName", ID, rowCount),					
							(CommonFunctions.getTestData(FILEPATH_TESTDATA, "CashorMarketValue", ID, rowCount)),
							(CommonFunctions.getTestData(FILEPATH_TESTDATA, "type", ID, rowCount)),
							(CommonFunctions.getTestData(FILEPATH_TESTDATA, "IsMultiple", ID,rowCount)));
			
			
			
			
			break;
			
			
			
		case "Retirement Fund" :
			OneRev.Assets(categoryValue);
			OneRev.RetirementFund(
					CommonFunctions.getTestData(FILEPATH_TESTDATA, "RetirementFundSourceName", ID, rowCount),
					(CommonFunctions.getTestData(FILEPATH_TESTDATA, "VestedInterestinRetirementFund", ID, rowCount)),
					(CommonFunctions.getTestData(FILEPATH_TESTDATA, "IsMultiple", ID,rowCount)));
							
			
			break;
			
			
		case "Business" :
			OneRev.Assets(categoryValue);
			OneRev.Business(
					CommonFunctions.getTestData(FILEPATH_TESTDATA, "BusinessSourceName", ID, rowCount),
					(CommonFunctions.getTestData(FILEPATH_TESTDATA, "NetWorthBusinessOwned", ID, rowCount)),
					(CommonFunctions.getTestData(FILEPATH_TESTDATA, "IsMultiple", ID,rowCount)));
							
			break;
		}
		
	
		return rs;
	
	}

	public boolean nextAssetData(int i) throws Exception {
		String categoryValue=CommonFunctions.getTestData1(FILEPATH_TESTDATA, "Category"+i, ID);
		
		switch (categoryValue) {	
		case "Life Insurance"	:	
			OneRev.Assets(categoryValue);
			OneRev.lifeInsurance(
					(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "AssetSourceName"+i, ID)),
							(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "FaceAmount"+i, ID)),
							(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "IsMultiple"+i, ID)));	
			   /*    returnVal= isNextDataPresent( i);*/
					
					break;
					
					
					
		case "Bank/Credit Union" :
			OneRev.Assets(categoryValue);
			OneRev.BankCreditUnion(
					CommonFunctions.getTestData1(FILEPATH_TESTDATA, "BankSourceName"+i, ID),
					(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "NameofBank"+i, ID)),
							(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "Accountnumber"+i, ID)),
							
							(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "CashorMarketValue"+i, ID)),
							(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "type"+i, ID)),
							(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "IsMultiple"+i, ID)));
			
			 /* returnVal= isNextDataPresent( i);*/
			
			break;
			
			
			
		case "Stock & Bonds" :
			OneRev.Assets(categoryValue);
			
			OneRev.StockBonds(
					CommonFunctions.getTestData1(FILEPATH_TESTDATA, "StockSourceName"+i, ID),					
							(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "CashorMarketValue"+i, ID)),
							(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "Stocktype"+i, ID)),
							(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "IsMultiple"+i, ID)));
			
			  /*returnVal=isNextDataPresent( i);*/
			
			
			break;
			
			
			
		case "Retirement Fund" :
			OneRev.Assets(categoryValue);
			OneRev.RetirementFund(
					CommonFunctions.getTestData1(FILEPATH_TESTDATA, "RetirementFundSourceName"+i, ID),
					(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "VestedInterestinRetirementFund"+i, ID)),
					(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "IsMultiple"+i, ID)));
							
			  /*returnVal= isNextDataPresent( i);*/
			break;
			
			
		case "Business" :
			OneRev.Assets(categoryValue);
			OneRev.Business(
					CommonFunctions.getTestData1(FILEPATH_TESTDATA, "BusinessSourceName"+i, ID),
					(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "NetWorthBusinessOwned"+i, ID)),
					(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "IsMultiple"+i, ID)));
			  /*returnVal=isNextDataPresent( i);	*/		
			break;
		}
		
		return false;
		
	}
	
/*	public boolean isNextDataPresent(int i) throws EncryptedDocumentException, InvalidFormatException, IOException {
		String nextDataPresent=(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "IsMultiple"+i, ID));
		if(nextDataPresent.equalsIgnoreCase("N")) {
			return false;
		}
		else {
			i++;
			return true;
		}
	}	*/	
				
	}
	
	
	


