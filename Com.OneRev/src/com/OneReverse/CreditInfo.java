package com.OneReverse;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.DriverScript.MasterScript;
import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.FPath;
import com.UtilityFiles.ReportStatus;
public class CreditInfo {
	static com.UtilityFiles.CommonFunctions data;
	public static final String FILEPATH_TESTDATA = FPath.TESTDATA;
	public static final String ID = "com.OneReverse.CreditInfo";
	ReportStatus rs = new ReportStatus();
	public  ReportStatus CreditInfo(Integer rowCount) throws EncryptedDocumentException, Exception, IOException {
		//String categoryValue=CommonFunctions.getTestData(FILEPATH_TESTDATA, "Category", ID);
		rs.setReportStatus(false);
		OneRev.CreditInfo(CommonFunctions.getTestData(FILEPATH_TESTDATA, "Cindicator", ID, rowCount));
		
		
		getNextRowAvalibility(rowCount);
		return rs;
		
	}
	
	private void getNextRowAvalibility(Integer rowCount)
			throws EncryptedDocumentException, InvalidFormatException, IOException {
		// TODO Auto-generated method stub
		FileInputStream fileInputStream = new FileInputStream(FILEPATH_TESTDATA);
		Workbook wb = WorkbookFactory.create(fileInputStream);
		org.apache.poi.ss.usermodel.Sheet s = wb.getSheet(MasterScript.sheetName);
		org.apache.poi.ss.usermodel.Row row = s.getRow(rowCount++);
		if (row != null) {
			rs.setNextRowPresent(true);
		}

	}
}