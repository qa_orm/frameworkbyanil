package com.UtilityFiles;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;



public class CommonUtils {
	public static WebDriver drv=null;
	public WebDriver getDriverObject() throws InterruptedException
	{
		
	
	  String brwName = Constants.BRW_NAME1;
	
	  switch(brwName)
	  {
	  case "ie":
		 String path = "D:\\Automation\\Selenium\\Drivers\\IEDriverServer.exe"; 
		 System.setProperty("webdriver.ie.driver",path);
		 drv = new InternetExplorerDriver();
		 break;
	  case "firefox":
		 System.setProperty("webdriver.gecko.driver", "D:\\Automation\\Selenium\\Drivers\\geckodriver.exe");
	     drv = new FirefoxDriver();
	     break;
	  case "chrome":
		String path1 = "./executables/chromedriver.exe";  
		System.setProperty("webdriver.chrome.driver",path1);
		drv = new ChromeDriver();
		break;
	   default:
		   System.out.println("Invalid Browser");
	  }
	   
	  drv.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	  Thread.sleep(3000);
	  drv.manage().window().maximize();
	  Thread.sleep(3000);
	  drv.get(Constants.URL);
	  Thread.sleep(3000);
	  
	 return drv; 
	}

}
