package com.DriverScript;

import java.io.FileInputStream;
import java.util.Date;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.Test;

import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.FPath;
import com.UtilityFiles.GenericLibrary;
import com.UtilityFiles.ReportStatus;

public class MasterScript {

	public static String GlobalScript;
	public static String ScreenShotPath;
	public static String ConstantTimeStamp;
	public static String Logpath;
	public static String sheetName;
	static int TotalPass = 0, TotalFail = 0, TotalSkip = 0;
	public static String scriptName;
	public static String SourceSystem;
	public static String MemId;
	public static int Counter = 0;

	/*
	 * public static void main(String[] args) throws Exception { MasterScript ms=new
	 * MasterScript(); ms.RunTestSuite(); }
	 */

	@Test
	public void executeScript() throws Exception {
		int rowCount = 1;
		boolean isNextRow = true;
		MasterScript ms = new MasterScript();
		while (isNextRow) {
			isNextRow = ms.RunTestSuite(rowCount);
			rowCount++;
		}
	}

	public boolean RunTestSuite(Integer rowCount) throws Exception {
		long SuiteStop = 0;
		long SuiteDuration = 0;
		String Duration = "";
		ReportStatus repStatus = new ReportStatus();

		try {
			ConstantTimeStamp = GenericLibrary.getTimeStamp();
			String ResDir = GenericLibrary.createResultFolder();
			GenericLibrary.createLogFolder();
			ScreenShotPath = GenericLibrary.createScreenShotFolder();
			GenericLibrary.copyFile();
			long SuiteStart = new Date().getTime();// Start Time for entire suite
			// Reading Configuration Excel
			FileInputStream fileInputStream = new FileInputStream(FPath.CONFIGURATION);
			Workbook wb = WorkbookFactory.create(fileInputStream);
			Sheet s = wb.getSheet("ORM");
			int rowCnt = s.getLastRowNum();
			GlobalScript = FPath.LOGS + MasterScript.ConstantTimeStamp + "/" + "mytest02" + ".xlsx";
			GenericLibrary.Create_LogFile("mytest02");// point 1
			for (int j = 1; j <= rowCnt; j++) {
				Counter = 0;
				org.apache.poi.ss.usermodel.Row row = s.getRow(j);
				org.apache.poi.ss.usermodel.Cell cell = row.getCell(1);
				String execStatus = cell.getRichStringCellValue().toString();
				if (execStatus.equalsIgnoreCase("yes")) {
					org.apache.poi.ss.usermodel.Cell cell1 = row.getCell(0);
					String scrptNm1 = cell1.getRichStringCellValue().toString();
					sheetName = scrptNm1;
					String filePath = FPath.TESTDATA;
					System.out.println("Path test::" + filePath);
					String testId[] = CommonFunctions.getTestCaseId(filePath, rowCount);
					String testId1[] = CommonFunctions.getTestCaseId1(filePath, rowCount);

					for (String id : testId) {
						scriptName = id;
						System.out.println(scriptName + " " + scrptNm1);
						SourceSystem = testId1[Counter];
						MemId = testId1[Counter + 1];

						int indexNo = scriptName.lastIndexOf(".") + 1;
						String methodName = scriptName.substring(indexNo);

						GlobalScript = FPath.LOGS + MasterScript.ConstantTimeStamp + "/" + "mytest02" + ".xlsx";
						Class<?> c;
						String className = methodName;
						long startTime = new Date().getTime();
						c = Class.forName(scriptName); // ClassName
						java.lang.reflect.Constructor<?> cons = c.getConstructor();
						Object object = cons.newInstance();
						Class[] cArg = new Class[1];
						cArg[0] = Integer.class;
						java.lang.reflect.Method method = object.getClass().getMethod(className, cArg);
						// System.out.println("name of class" +className);
						// java.lang.reflect.Method method= c.getMethod(className);
						// Boolean bool = (Boolean) method.invoke(object);
						repStatus = (ReportStatus) method.invoke(object, rowCount);
						long stopTime = new Date().getTime(); // Stop Time for a individual test
						// Casting of object to boolean
						boolean b = repStatus.isReportStatus();
						SuiteStop = new Date().getTime(); // End Time for entire suite
						SuiteDuration = (SuiteStop - SuiteStart) / 1000;
						Duration = GenericLibrary.getTime(SuiteDuration);
						Counter = Counter + 2;

						if (b == false) // To Check complete test case Pass/Fail
						{
							// rowCount++;
							GenericLibrary.setResultCell(className, "Pass");
							GenericLibrary.CreateHTMLReport(Duration, ConstantTimeStamp, ConstantTimeStamp);
						} else {
							GenericLibrary.setResultCell(className, "Fail");
							GenericLibrary.CreateHTMLReport(Duration, ConstantTimeStamp, ConstantTimeStamp);
						}
					}

				}
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return repStatus.isNextRowPresent();
	}

}
