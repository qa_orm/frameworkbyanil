package com.TestCases;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Specific_Methods.TestCase_Specific_Methods;
import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.CommonUtils;
import com.UtilityFiles.Constants;
import com.UtilityFiles.FPath;



public class VerifyPatientInfo {
	FPath FP;
	CommonFunctions common;
	CommonUtils com;
	TestCase_Specific_Methods spm;
	WebDriver dr;
	Constants con;
    CommonUtils com1 = new CommonUtils();
    public ArrayList<Boolean> statusList1=new ArrayList<Boolean>();
		
	
	
	public boolean VerifyPatientInfo() throws InterruptedException{
		preCond();
		VerifyPatientInformation();
		postCond();
		statusList1.addAll(spm.statusList);
		return statusList1.contains(false);
		
	}
	
   
	  public boolean preCond() throws InterruptedException
	  {
		try {
			FP=new FPath();
			common=new CommonFunctions();
			spm=new TestCase_Specific_Methods();
			//dr=com.getDriverObject();
			com1.getDriverObject();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	  }
	
    
	public boolean VerifyPatientInformation()
	{
		boolean tc_res = false;
		try {
		    spm.SelectProfile(com1.drv);
			spm.LoginBriovaRx(com1.drv, Constants.ID, Constants.PWD);
			Thread.sleep(5000);
			spm.Patient_Information_Name(com1.drv);
			spm.Patient_DOB(com1.drv);
			spm.Patient_EmailAddress(com1.drv);
			spm.Patient_ID(com1.drv);
			spm.Gender(com1.drv);
			spm.Address(com1.drv);
			spm.LogoutBriovaRx(com1.drv);
		} catch (Exception e)
		{
			Reporter.log("Error " + e.getMessage()); 	
			tc_res = false;
		}
		return tc_res;
	}
	
    
	  public boolean postCond()
	  {
		  try {
			com1.drv.close();
			  com1.drv.quit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  return false;
	  }
}
