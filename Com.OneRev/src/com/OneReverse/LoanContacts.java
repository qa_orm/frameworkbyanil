package com.OneReverse;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.FPath;
public class LoanContacts {
	static com.UtilityFiles.CommonFunctions data;
	public static final String FILEPATH_TESTDATA = FPath.TESTDATA;
	public static final String ID = "com.OneReverse.LoanContacts";

	public  boolean LoanContacts(Integer rowCount) throws EncryptedDocumentException, Exception, IOException {
		//String categoryValue=CommonFunctions.getTestData(FILEPATH_TESTDATA, "Category", ID);
		
		OneRev.LoanContacts(CommonFunctions.getTestData(FILEPATH_TESTDATA, "LoanOfficer", ID, rowCount),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "LoanProcessor", ID, rowCount)),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "PreferredContact", ID, rowCount)));
		
		
		
		return false;
		
	}
}

