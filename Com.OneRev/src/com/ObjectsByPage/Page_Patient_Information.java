package com.ObjectsByPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Page_Patient_Information {
	public Page_Patient_Information(WebDriver drv) {
		PageFactory.initElements(drv, this);
	}
	
	//Patient Name  
	@FindBy(xpath="//div[@id='patientInfoPrint']/div[1]/div[1]/fieldset[1]")
	private WebElement Name;
	
	public WebElement NameField()
	{
		return Name;
	}
	
	//Patient Birth Date  
		@FindBy(xpath="//div[@id='patientInfoPrint']/div/div[2]/fieldset[1]")
		private WebElement Birth_Date;
		
		public WebElement Birth_Date()
		{
			return Birth_Date;
		}
		
		//Patient Email Address  
		@FindBy(xpath="//span[@id='patientActiveEmailAddress']")
		private WebElement Email_Address;
		
		public WebElement Email_Address()
		{
			return Email_Address;
		}	
		
		//Patient Patient ID  
				@FindBy(xpath="//div[@id='patientInfoPrint']/div/div[1]/fieldset[2]")
				private WebElement Patient_ID;
				
				public WebElement Patient_ID()
				{
					return Patient_ID;
				}
				
	 //Patient Gender  
				@FindBy(xpath="//div[@id='patientInfoPrint']/div/div[2]/fieldset[2]")
				private WebElement Gender;
				
				public WebElement Gender()
				{
					return Gender;
				}
	
	//Patient Address  //a[@id='refillButton']
				@FindBy(xpath="//div[@id='patientInfoPrint']/div/div[3]/fieldset[2]")
				private WebElement Address;
				
				public WebElement Address()
				{
					return Address;
				}
				
//Patient View Refillable Prescription   
				@FindBy(xpath="//a[@id='refillButton']")
				private WebElement refill;
				
				public WebElement View_Refillable()
				{
					return refill;
				}				
	
}
