package com.OneReverse;
import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.FPath;
import com.UtilityFiles.ReportStatus;
public class Income {
	static com.UtilityFiles.CommonFunctions data;
	public static final String FILEPATH_TESTDATA = FPath.TESTDATA;
	public static final String ID = "com.OneReverse.Income";

//	public  boolean Income(Integer rowCount) throws EncryptedDocumentException, Exception, IOException {
	ReportStatus rs=new ReportStatus();
	public ReportStatus Income(Integer rowCount) throws Exception {
		
		rs.setReportStatus(false);

		OneRev.Income(CommonFunctions.getTestData(FILEPATH_TESTDATA, "SourceName", ID,rowCount),

				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "Incomevalue", ID,rowCount)),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "SocialSecuritySourceName", ID,rowCount)),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "SocialSecurityIncomevalue", ID,rowCount)),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "DividendSourceName", ID,rowCount)),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "DividendIncomevalue", ID,rowCount)),
				//(CommonFunctions.getTestData(FILEPATH_TESTDATA, "EmploymentSource", ID,rowCount)),
				//(CommonFunctions.getTestData(FILEPATH_TESTDATA, "EmploymentIncomevalue", ID,rowCount)),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "RentalSource", ID,rowCount)),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "RentalIncomevalue", ID,rowCount)),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "OtherformsofIncome", ID,rowCount)),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "OtherformsofSource", ID,rowCount)),
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "Otherincome", ID,rowCount)) ,
				(CommonFunctions.getTestData(FILEPATH_TESTDATA, "IsMultiple", ID,rowCount)));
		
		
		
	
		
		return rs;

	/*	String nextRowPresent= CommonFunctions.getTestData(FILEPATH_TESTDATA, "IsMultiple", ID,rowCount);
		if(nextRowPresent.equalsIgnoreCase("Y"))
		{							
			Income();
		}
*/
	
		
	}
	
	public  boolean OthersFormsOfIncome(Integer i) throws EncryptedDocumentException, Exception, IOException {


		OneRev.OthersFormsOfIncome(
				(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "OtherformsofIncome"+i, ID)),
				(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "OtherformsofSource"+i, ID)),
				(CommonFunctions.getTestData1(FILEPATH_TESTDATA, "Otherincome"+i, ID))
				);
		String nextRowPresent= CommonFunctions.getTestData1(FILEPATH_TESTDATA, "IsMultiple"+i, ID);
		if(nextRowPresent.equalsIgnoreCase("N"))
		{							
			return false;
		}


		return true;
	}
}
	

	
	