package com.TestCases;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import com.Specific_Methods.TestCase_Specific_Methods;
import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.CommonUtils;
import com.UtilityFiles.Constants;
import com.UtilityFiles.FPath;

public class UpdatePhoneNum {
	FPath FP;
	CommonFunctions common;
	CommonUtils com;
	TestCase_Specific_Methods spm;
	WebDriver dr;
	Constants con;
	CommonUtils com1 = new CommonUtils();
	public ArrayList<Boolean> statusList1=new ArrayList<Boolean>();
		
	
	
	public boolean UpdatePhoneNum() throws InterruptedException{
		preCond();
		UpdatePhone();
		postCond();
		statusList1.addAll(spm.statusList);
		return statusList1.contains(false);
		
	}
	  public boolean preCond() throws InterruptedException
	  {
		try {
			com1.getDriverObject();
			FP=new FPath();
			common=new CommonFunctions();
			spm=new TestCase_Specific_Methods();
			//dr=com.getDriverObject();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	  }
	
	public boolean UpdatePhone()
	{
		boolean tc_res = false;
		try {
			spm.SelectProfile(com1.drv);
					
			spm.LoginBriovaRx(com1.drv, Constants.ID, Constants.PWD);
			
			spm.UpdateMyProfileLink(com1.drv);
			
			spm.PhoneLink(com1.drv);
			spm.PhoneNumTxtFld(com1.drv);
			spm.SubmitButton(com1.drv);
			
			spm.SccMsgVerify(com1.drv);
			
			spm.LogoutBriovaRx(com1.drv);
			
		} catch (Exception e) {
			Reporter.log("Error " + e.getMessage()); 	
			tc_res = false;
		}
		return tc_res;
	}
	
	  public boolean postCond()
	  {
		  try {
			com1.drv.close();
			  com1.drv.quit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  return false;
	  }
	
}
