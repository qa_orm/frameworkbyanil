package com.OneReverse;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.UtilityFiles.GenericLibrary;
//import com.primeclerk.objectProp.Properties;
import com.primeclerk.objectProp.objLoginPage;

public class OneRev {

	public static System sys;
	public static WebDriver driver;
	public static String uniqueNum = "" + System.currentTimeMillis();

	public static ArrayList<Boolean> statusList = new ArrayList<Boolean>();
	static Boolean Sts = false;
	static String StepSts;

	public static final String ID = "com.OneReverse.Login";

	public static final String ID1 = "com.primeclerk.objLoginPage";

	static {
		sys.setProperty("webdriver.chrome.driver", "D:\\FrameWork\\Drivers\\chromedriver.exe");

		sys.setProperty("webdriver.ie.driver", "D:\\FrameWork\\Drivers\\IEDriverServer.exe");

	}

	public static void loadUrl(String url) {

		if (url.equals("IE")) {
			driver = new InternetExplorerDriver();
			driver.get("http://devcsa.primeclerk.com/CM/Home-Login");
			driver.manage().window().maximize();
		}

		if (url.equals("Chrome")) {
			driver = new ChromeDriver();
			driver.get("https://qasandbox-ormmortgageservices.cs23.force.com/s/");
			// + ""
			// +
			// "https://qasandbox-ormmortgageservices.cs17.force.com/PartnerLoginPage?startURL=%2Fs%2F%3Ft%3D1500881821229");
			driver.manage().window().maximize();
		}
	}

	public static void login(String userName, String password) throws Exception {

		// To wait until the element loads
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// To Maximize the window
		driver.manage().window().maximize();

		// Enter the username
		driver.findElement(objLoginPage.txtUsername).sendKeys(userName);

		if (StepSts != null) {
			GenericLibrary.setCellData("Enter the" + userName + " Username", "The user should enter the valid Username",
					"The user has NOT successfully enter the valid Username", "Fail");
			statusList.add(false);
			Sts = false;

		} else {

			GenericLibrary.setCellData("Enter the" + userName + " Username", "The user should enter the valid MSID",
					"The user has successfully enter the valid MSID", "Pass");
			Sts = true;
			statusList.add(true);

		}

		// Enter the password
		driver.findElement(objLoginPage.txtPassword).sendKeys(password);

		if (StepSts != null) {
			GenericLibrary.setCellData("Enter the " + password + " Password",
					"The user should enter the valid MSID Password",
					"The user has NOT successfully enter the valid MSID Password", "Fail");
			Sts = false;
			statusList.add(false);

		} else {
			GenericLibrary.setCellData("Enter the " + password + " Password",
					"The user should enter the valid MSID Password",
					"The user has successfully enter the valid MSID Password", "Pass");
			Sts = true;
			statusList.add(true);
		}

		// Click on submit button
		driver.findElement(objLoginPage.btnSubmit).click();
		//
		if (StepSts != null) {
			GenericLibrary.setCellData("Click on Login Button", "The user should click on Login Button",
					"The user has NOT click on Login Button", "Fail");
			Sts = false;
			statusList.add(false);

		} else {
			GenericLibrary.setCellData("Click the Login Button", "The user should click on Login Button",
					"The user has successfully clicked on Login Button", "Pass");
			Sts = true;
			statusList.add(true);

			// Enter the Click on Sanl link
			driver.findElement(objLoginPage.sanl).click();

			if (StepSts != null) {
				GenericLibrary.setCellData("Enter the" + userName + " Username",
						"The user should enter the valid Username",
						"The user has NOT successfully enter the valid Username", "Fail");
				statusList.add(false);
				Sts = false;

			} else {

				GenericLibrary.setCellData("Enter the" + userName + " Username", "The user should enter the valid MSID",
						"The user has successfully enter the valid MSID", "Pass");
			}
			Sts = true;
			statusList.add(true);
		}

	}

	public static void Appdate1(String appdate) throws Exception {

		// To wait until the element loads
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		// To Maximize the window
		// driver.manage().window().maximize();

		// Enter the username
		driver.findElement(objLoginPage.appdate1).sendKeys(appdate);

		// driver.findElement(objLoginPage.appdate1).sendKeys(appdate);
		driver.findElement(objLoginPage.appl).click();
		Thread.sleep(1000L);

		List<WebElement> Checkbox = driver.findElements(By.xpath("//input[@type='checkbox']"));

		for (int i = 0; i < Checkbox.size(); i++) {
			WebElement element = driver.findElement(By.xpath("//input[@type='checkbox']"));
			// ((JavascriptExecutor)
			// driver).executeScript("arguments[0].scrollIntoView(true);", element);
			Thread.sleep(500);
			WebElement El = Checkbox.get(i);
			String id = El.getAttribute("value");

			if (!El.isSelected()) {
				El.click();
			}

		} // End While

		// driver.findElement(objLoginPage.rb02).click();
		// driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		/*
		 * driver.findElement(objLoginPage.rb01).click(); Thread.sleep(1000L);
		 * 
		 * driver.findElement(objLoginPage.rb02).click(); Thread.sleep(1000L);
		 */
		driver.findElement(objLoginPage.Strtapp).click();

	}

	public static void Loan(String MortgageAppliedfor, String Slpplan, String EstimateofAppraisedValue, String Margin,
			String LoanOriginationFeeCalculationMethod, String Thisapplicationwastakenby,
			String LoanOriginatorsFirstName, String LoanOriginatorsLastName, String LoanOriginatorsPhoneNumber,
			String LoanOriginationCompanysName, String LoanOriginationCompanysAddress,
			String LoanOriginationCompanysCity, String LoanOriginationCompanysZipCode, String Childrenlivingunderage,
			String POA, String NameofPOA, String sqfootge, String AltcontactName, String AltcontactNamefulladdress,
			String AltcontactNamephno, String RelationshiptoAlternativeContact

	// 2nd field String PurposeofLoan commented out

	) throws Exception {

		// WebDriverWait wait = new WebDriverWait(driver, 10);
		// WebElement element =
		// wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'Mortgage
		// Applied for ')]/../following-sibling::select")));

		Thread.sleep(1000L);

		try {
			// Waits for 20 seconds
			WebDriverWait wait = new WebDriverWait(driver, 10);

			// Wait until expected condition size of the dropdown increases and becomes more
			// than 1
			wait.until((ExpectedCondition<Boolean>) new ExpectedCondition<Boolean>() {
				public Boolean apply(WebDriver driver) {
					Select select = new Select(driver.findElement(objLoginPage.Mrtgappfor));
					return select.getOptions().size() > 1;
				}
			});

			WebElement element = driver.findElement(objLoginPage.Mrtgappfor);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

			// To select the first option
			Select select = new Select(driver.findElement(objLoginPage.Mrtgappfor));
			select.selectByValue(MortgageAppliedfor);
		} catch (Throwable e) {
			System.out.println("Error found: " + e.getMessage());
		}

		Thread.sleep(1000L);

		// WebElement elem3 = driver.findElement(objLoginPage.Pol);

		// Select ele = new Select(elem3);

		// ele.selectByValue(PurposeofLoan);

		WebElement sel3 = driver.findElement(objLoginPage.Slpplan);

		Select sl3 = new Select(sel3);

		sl3.selectByValue(Slpplan);

		Thread.sleep(2000L);
		driver.findElement(objLoginPage.EAV).sendKeys(EstimateofAppraisedValue);

		Thread.sleep(2000L);

		if (StepSts != null) {
			GenericLibrary.setCellData("Enter the" + EstimateofAppraisedValue + " EstimateofAppraisedValue",
					"The user should enter the valid EstimateofAppraisedValue",
					"The user has NOT successfully entered the valid EstimateofAppraisedValue", "Fail");
			statusList.add(false);
			Sts = false;

		} else {

			GenericLibrary.setCellData("Enter the" + EstimateofAppraisedValue + " EstimateofAppraisedValue",
					"The user should enter the valid EstimateofAppraisedValue",
					"The user has successfully entered the valid EstimateofAppraisedValue", "Pass");
			Sts = true;
			statusList.add(true);

			WebElement sel4 = driver.findElement(objLoginPage.Margin);

			Select sl4 = new Select(sel4);

			sl4.selectByValue(Margin);

		}

		WebElement sel4 = driver.findElement(objLoginPage.LoanOriginationFeeCalculationMethod);

		Select sl4 = new Select(sel4);

		sl4.selectByValue(LoanOriginationFeeCalculationMethod);

		Thread.sleep(2000L);

		WebElement sel5 = driver.findElement(objLoginPage.Thisapplicationwastakenby);

		Select sl5 = new Select(sel5);

		sl5.selectByValue(Thisapplicationwastakenby);

		Thread.sleep(2000L);

		// driver.findElement(objLoginPage.LoanOriginatorsFirstName).sendKeys(LoanOriginatorsFirstName);
		// Thread.sleep(3000L);

		// driver.findElement(objLoginPage.LoanOriginatorsFirstName).click();
		// Thread.sleep(1000L);
		// driver.findElement(objLoginPage.LoanOriginatorsLastName).sendKeys(LoanOriginatorsLastName);
		// Thread.sleep(3000L);
		// driver.findElement(objLoginPage.LoanOriginatorsPhoneNumber).sendKeys(LoanOriginatorsPhoneNumber);
		// Thread.sleep(500L);
		// driver.findElement(objLoginPage.LoanOriginationCompanysName).sendKeys(LoanOriginationCompanysName);
		// Thread.sleep(1500L);
		// driver.findElement(objLoginPage.LoanOriginationCompanysAddress).sendKeys(LoanOriginationCompanysAddress);
		// Thread.sleep(1500L);
		// driver.findElement(objLoginPage.LoanOriginationCompanysCity).sendKeys(LoanOriginationCompanysCity);
		// Thread.sleep(2000L);

		//// ---------------------------------------state--------------------------------

		// Wait until expected condition size of the dropdown increases and becomes more
		// than 1
		// wait.until((ExpectedCondition<Boolean>) new ExpectedCondition<Boolean>(){
		// public Boolean apply(WebDriver driver)

		// Select select = new
		// Select(driver.findElement(By.xpath(LoanOriginationCompanysState)));
		// return select.getOptions().size()>1;

		// WebElement element =
		// driver.findElement(objLoginPage.LoanOriginationCompanysState);
		// ((JavascriptExecutor)
		// driver).executeScript("arguments[0].scrollIntoView(true);", element);

		// WebElement element1 =
		// driver.findElement(objLoginPage.LoanOriginationCompanysState);
		// Select oSelect = new Select(element1);

		// oSelect.selectByIndex(5);

		// Or it can be also written as

		// driver.findElement(objLoginPage.LoanOriginationCompanysState).sendKeys(LoanOriginationCompanysState);
		Thread.sleep(2000L);
		// driver.findElement(objLoginPage.LoanOriginatorIdentifier).sendKeys(LoanOriginatorIdentifier);
		// Thread.sleep(500L);
		// driver.findElement(objLoginPage.LoanOriginationCompanyIdentifier).sendKeys(LoanOriginationCompanyIdentifier);

		// driver.findElement(objLoginPage.LoanOriginationCompanysZipCode).sendKeys(LoanOriginationCompanysZipCode);

		Thread.sleep(500L);

		WebElement element11 = driver.findElement(objLoginPage.household);
		Select oSelect1 = new Select(element11);

		oSelect1.selectByIndex(5);

		Thread.sleep(1500L);

		WebElement element111 = driver.findElement(objLoginPage.Childrenlivingunderage);
		Select oSelect11 = new Select(element111);

		oSelect11.selectByIndex(2);

		Thread.sleep(1000L);
		WebElement element1111 = driver.findElement(objLoginPage.POA);
		Select oSelect12 = new Select(element1111);

		oSelect12.selectByValue(POA);

		Thread.sleep(1000L);

		driver.findElement(objLoginPage.NameofPOA).sendKeys(NameofPOA);

		driver.findElement(objLoginPage.sqfootge).sendKeys(sqfootge);
		Thread.sleep(1000L);
		driver.findElement(objLoginPage.AltcontactName).sendKeys(AltcontactName);

		Thread.sleep(1000L);
		driver.findElement(objLoginPage.AltcontactNamefulladdress).sendKeys(AltcontactNamefulladdress);
		Thread.sleep(1000L);
		driver.findElement(objLoginPage.AltcontactNamephno).sendKeys(AltcontactNamephno);

		Thread.sleep(1000L);
		WebElement element1112 = driver.findElement(objLoginPage.rlshpaltcont);
		Select oSelect13 = new Select(element1112);

		oSelect13.selectByValue(RelationshiptoAlternativeContact);

		Thread.sleep(2000L);

		JavascriptExecutor je = (JavascriptExecutor) driver;

		// Identify the WebElement which will appear after scrolling down

		WebElement save = driver.findElement(By.xpath("//span[contains(.,'Save and Continue')]"));

		// now execute query which actually will scroll until that element is not
		// appeared on page.

		je.executeScript("arguments[0].scrollIntoView(true);", save);

		driver.findElement(By.xpath("//span[contains(.,'Save and Continue')]")).click();

	}

	public static void Subjectproperty(String SubjectPropertyAddress, String SubjectPropertycity,
			String SubjectPropertyZipCode, String LegalDescriptionofProperty, String ResidenceType, String noofunits,

			String yearbuilt, String propertyheldas, String PropertyTitleisHeldinTheseNames,
			String RealEstateTaxesMonthly, String HazardInsuranceMonthly, String misc, Object object, Object obj)

			throws InterruptedException {
		// TODO Auto-generated method stub

		driver.findElement(objLoginPage.SubjectPropertyAddress).sendKeys(SubjectPropertyAddress);

		Thread.sleep(1000L);
		driver.findElement(objLoginPage.SubjectPropertycity).sendKeys(SubjectPropertycity);

		driver.findElement(objLoginPage.SubjectPropertyZipCode).sendKeys(SubjectPropertyZipCode);

		Thread.sleep(1000L);

		driver.findElement(objLoginPage.LegalDescriptionofProperty).sendKeys(LegalDescriptionofProperty);

		Thread.sleep(1000L);

		WebElement element1112 = driver.findElement(objLoginPage.ResidenceType);
		Select oSelect13 = new Select(element1112);

		oSelect13.selectByValue(ResidenceType);

		Thread.sleep(2000L);

		driver.findElement(objLoginPage.noofunits).sendKeys(noofunits);

		Thread.sleep(1000L);
		driver.findElement(objLoginPage.yearbuilt).sendKeys(yearbuilt);

		Thread.sleep(1500L);

		// JavascriptExecutor je = (JavascriptExecutor) driver;
		// WebElement scr = driver.findElement(objLoginPage.propertyheldas);

		// now execute query which actually will scroll until that element is not
		// appeared on page.

		// je.executeScript("arguments[0].scrollIntoView(true);",scr);

		WebElement element1113 = driver.findElement(objLoginPage.propertyheldas);
		Select oSelect14 = new Select(element1113);

		oSelect14.selectByValue(propertyheldas);

		Thread.sleep(1000L);

		driver.findElement(objLoginPage.PropertyTitleisHeldinTheseNames).sendKeys(PropertyTitleisHeldinTheseNames);

		Thread.sleep(3000L);

		WebElement element1114 = driver.findElement(objLoginPage.CheckIfTitleisalsoHeldAs);
		Select oSelect15 = new Select(element1114);

		oSelect15.selectByIndex(1);

		Thread.sleep(1000L);

		driver.findElement(objLoginPage.RealEstateTaxesMonthly).sendKeys(RealEstateTaxesMonthly);
		Thread.sleep(1000L);

		driver.findElement(objLoginPage.HazardInsuranceMonthly).sendKeys(HazardInsuranceMonthly);

		Thread.sleep(1000L);

		driver.findElement(objLoginPage.misc).sendKeys(misc);
		Thread.sleep(1000L);

		driver.findElement(By.xpath("//span[contains(.,'Save and Continue')]")).click();

		Thread.sleep(1500L);

		driver.findElement(By.xpath("//span[contains(.,'Add Client')]")).click();

		Thread.sleep(1000L);

	}

	public static void Client(String FirstName, String MiddleName, String LastName, String CurrentAddress,
			String CurrentCity, String CurrentState, String CurrentZip, String NoofYrsCurrentAddress, String email,
			String HomePhone, String MobilePhone, String DateofBirth, String SSN, String MaritalStatus, String NBS,
			String NBSName, String NBSPhone, String NBSdob, String NBSRel

	) throws InterruptedException {

		driver.findElement(objLoginPage.FirstName).sendKeys(FirstName);
		Thread.sleep(1000L);
		driver.findElement(objLoginPage.MiddleName).sendKeys(MiddleName);
		Thread.sleep(1000L);
		driver.findElement(objLoginPage.LastName).sendKeys(LastName);
		Thread.sleep(3000L);
		driver.findElement(objLoginPage.CurrentAddress).sendKeys(CurrentAddress);
		Thread.sleep(3000L);
		driver.findElement(objLoginPage.CurrentCity).sendKeys(CurrentCity);
		Thread.sleep(3000L);

		WebElement element1116 = driver.findElement(objLoginPage.CurrentState);
		Select oSelect16 = new Select(element1116);

		oSelect16.selectByValue(CurrentState);

		Thread.sleep(1000L);
		driver.findElement(objLoginPage.CurrentZip).sendKeys(CurrentZip);

		Thread.sleep(1000L);
		driver.findElement(objLoginPage.NoofYrsCurrentAddress).sendKeys(NoofYrsCurrentAddress);

		Thread.sleep(1000L);

		driver.findElement(
				By.xpath("//input[@type='checkbox' and following-sibling::label/span = 'Client has Email Address?']"))
				.click();

		Thread.sleep(1000L);
		driver.findElement(objLoginPage.email).sendKeys(email);

		Thread.sleep(1000L);
		driver.findElement(objLoginPage.HomePhone).sendKeys(HomePhone);

		Thread.sleep(1000L);
		driver.findElement(objLoginPage.MobilePhone).sendKeys(MobilePhone);

		Thread.sleep(1000L);
		driver.findElement(objLoginPage.DateofBirth).sendKeys(DateofBirth);
		Thread.sleep(1000L);
		driver.findElement(objLoginPage.SSN).sendKeys(SSN);
		// TODO Auto-generated method stub

		Thread.sleep(1500L);

		WebElement element1117 = driver.findElement(objLoginPage.MaritalStatus);
		Select oSelect17 = new Select(element1117);

		oSelect17.selectByValue(MaritalStatus);

		Thread.sleep(1500L);

		driver.findElement(By.xpath(
				"//input[@type='checkbox' and following-sibling::label/span = 'Mailing Address same as Present Address']"))
				.click();

		Thread.sleep(3000L);

		WebElement element1118 = driver.findElement(objLoginPage.NBS);
		Select oSelect18 = new Select(element1118);
		oSelect18.selectByValue(NBS);

		Thread.sleep(1000L);

		driver.findElement(objLoginPage.NBSName).sendKeys(NBSName);
		Thread.sleep(1000L);

		driver.findElement(objLoginPage.NBSPhone).sendKeys(NBSPhone);

		Thread.sleep(1000L);

		driver.findElement(objLoginPage.NBSdob).sendKeys(NBSdob);

		Thread.sleep(1000L);
		driver.findElement(objLoginPage.NBSRel).sendKeys(NBSRel);

		driver.findElement(By.xpath(
				"//input[@type='checkbox' and following-sibling::label/span = 'Mailing Address same as Spouse Present Address (above)']"))
				.click();

		driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();

		Thread.sleep(3500L);

		driver.findElement(By.xpath("//span[contains(.,'Next')]")).click();

		Thread.sleep(3500L);
		// driver.findElement(By.xpath("//span[contains(.,'Add')]")).click();

	}

	public static void Employment(String selfemployed, String IncomeType, String EmployerName, String PosTitle,
			String EmployerPhone, String EmployerAddress, String City, String State, String Zip, String Years,
			String Months, String YearsEmployed) throws Exception {

		WebElement element1119 = driver.findElement(objLoginPage.selfemployed);
		Select oSelect19 = new Select(element1119);

		oSelect19.selectByValue(selfemployed);

		Thread.sleep(1000L);

		WebElement element1120 = driver.findElement(objLoginPage.IncomeType);
		Select oSelect20 = new Select(element1120);

		oSelect20.selectByValue(IncomeType);

		Thread.sleep(1000L);

		driver.findElement(objLoginPage.EmployerName).sendKeys(EmployerName);

		Thread.sleep(1000L);

		driver.findElement(objLoginPage.PosTitle).sendKeys(PosTitle);

		Thread.sleep(1000L);

		driver.findElement(objLoginPage.EmployerPhone).sendKeys(EmployerPhone);

		Thread.sleep(1000L);

		driver.findElement(objLoginPage.EmployerAddress).sendKeys(EmployerAddress);

		Thread.sleep(3000L);

		driver.findElement(objLoginPage.City).sendKeys(City);
		Thread.sleep(3000);

		WebElement element1121 = driver.findElement(objLoginPage.State);
		Select oSelect21 = new Select(element1121);

		oSelect21.selectByValue(State);
		Thread.sleep(3000);

		driver.findElement(objLoginPage.Zip).sendKeys(Zip);

		Thread.sleep(3000);

		driver.findElement(objLoginPage.Years).sendKeys(Years);
		Thread.sleep(3000);

		driver.findElement(objLoginPage.Months).sendKeys(Months);

		Thread.sleep(3000);
		WebElement element1122 = driver.findElement(objLoginPage.YearsEmployed);
		Select oSelect22 = new Select(element1122);

		oSelect22.selectByValue(YearsEmployed);

		driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();

		Thread.sleep(3000L);

		JavascriptExecutor je = (JavascriptExecutor) driver;

		// Identify the WebElement which will appear after scrolling down

		WebElement next = driver.findElement(By.xpath("//span[contains(.,'Next')]"));

		// now execute query which actually will scroll until that element is not
		// appeared on page.

		je.executeScript("arguments[0].scrollIntoView(true);", next);

		driver.findElement(By.xpath("//span[contains(.,'Next')]")).click();

		Thread.sleep(1000L);

		System.out.println("executed edit line");

	}

	public static void Income(String SourceName, String Incomevalue, String SocialSecuritySourceName,
			String SocialSecurityIncomevalue, String DividendSourceName, String DividendIncomevalue,
			String RentalSource, String RentalIncomevalue, String OtherformsofIncome, String OtherformsofSource,
			String Otherincome, String isMultipleRows) throws EncryptedDocumentException, IOException, Exception {
		// TODO Auto-generated method stub

		
		
		//driver.findElement(By.linkText("Edit"))[position()=1])).click

		
		
		// List <WebElement> alllinks = driver.findElements(By.tagName("a"));

		/*
		 * for(int i=0;i<alllinks.size();i++)
		 * System.out.println(alllinks.get(i).getText());
		 * 
		 * for(int i=0;i<alllinks.size();i++){ // alllinks.get(i).click();
		 * 
		 * alllinks.get(1).click();
		 * 
		 * 
		 * }
		 */
		// extract the link texts of each link element
/*
		 @SuppressWarnings("unchecked")
		 List<WebElement> links = (List<WebElement>)
		 driver.findElement(By.xpath("//a[contains(text(),'Edit')]"));
		 for (WebElement link: links) {
		 link.click();*/
		 
		 
		List<WebElement> alllinks = driver.findElements(By.linkText("Edit"));
		List<String> a = new ArrayList<String>();
		for (int i = 0; i < alllinks.size(); i++) {
			List<WebElement> alllinks1 = driver.findElements(By.linkText("Edit"));		
			a.add(alllinks1.get(i).getText());
			// if(a.get(i).trim().equalsIgnoreCase("Edit"))
			alllinks1.get(i).click();
				int j = i + 1;
				switch (j) {
				case 1:
					editMethod1(SourceName, Incomevalue);
					break;
				case 2:
					editMethod2(SocialSecuritySourceName, SocialSecurityIncomevalue);
					break;
				case 3:
					editMethod3(DividendSourceName, DividendIncomevalue);
					break;
				case 4:
					editMethod4(RentalSource, RentalIncomevalue);
					break;
				default:
				break;
				}
			
			}
		

		// List<WebElement> li = driver.findElements(By.linkText("Edit"));
		// li.get(0).click();

		// driver.findElement(By.xpath(
		// "html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[5]/table[1]/tbody/tr[1]/td[6]/a
		// "html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/table[1]/tbody/tr[1]/td[6]/a"))
		// .click();

		// driver.findElement(By.linkText(a[i])).click();

		// alllinks.get(1).click();

		// List<WebElement> li1 = driver.findElements(By.linkText("Edit"));
		// li1.get(1).click();
		// driver.findElement(By.xpath(
		// "html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/table[1]/tbody/tr[1]/td[6]/a"))
		// .click();

		// li1.get(2).click();
		// alllinks.get(2).click();
		// Thread.sleep(5000);
		// driver.findElement(By.xpath(
		// "html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/table[1]/tbody/tr[1]/td[6]/a"))
		// .click();

		// alllinks.get(3).click();

		// driver.findElement(By.xpath(
		// "html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/table[1]/tbody/tr[1]/td[6]/a"))
		// .click();
		//
		// driver.findElement(objLoginPage.SourceName4).sendKeys(EmploymentSource);

		// driver.findElement(objLoginPage.Incomevalue4).clear();
		// Thread.sleep(5000);
		// driver.findElement(objLoginPage.Incomevalue4).sendKeys(EmploymentIncomevalue);
		// Thread.sleep(5000);
		// driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();
		// Thread.sleep(5000);

		// alllinks.get(4).click();
		// driver.findElement(By.xpath(
		// "html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[6]/table[1]/tbody/tr[5]/td[6]/a"))
		// .click();

		OthersFormsOfIncome(OtherformsofIncome, OtherformsofSource, Otherincome);
		if (isMultipleRows.equalsIgnoreCase("N")) {
			driver.findElement(By.xpath("//span[contains(.,'Next')]")).click();
			Thread.sleep(3000);
		} else if (isMultipleRows.equalsIgnoreCase("Y")) {
			int i = 1;
			boolean nextIncome = new Income().OthersFormsOfIncome(i);
			while (nextIncome) {
				nextIncome = new Income().OthersFormsOfIncome(i);
				if (!nextIncome) {
					break;
				}
				i++;
			}
			driver.findElement(By.xpath("//span[contains(.,'Next')]")).click();
			Thread.sleep(3000);
		
		}}
	

	public static void Assets(String Category) throws InterruptedException {
		// TODO Auto-generated method stub

		// driver.findElement(objLoginPage.Category).sendKeys(Category);
		// Thread.sleep(3500);

		WebElement element1122 = driver.findElement(objLoginPage.Category);
		Select oSelect22 = new Select(element1122);

		oSelect22.selectByValue(Category);

	}

	/*
	 * JavascriptExecutor je = (JavascriptExecutor) driver;
	 * 
	 * //Identify the WebElement which will appear after scrolling down
	 * 
	 * WebElement add = driver.findElement(By.xpath("//span[contains(.,'Add')]"));
	 * 
	 * // now execute query which actually will scroll until that element is not
	 * appeared on page.
	 * 
	 * je.executeScript("arguments[0].scrollIntoView(true);",add);
	 * 
	 * driver.findElement(By.xpath("//span[contains(.,'Add')]")).click();
	 * Thread.sleep(1500);
	 * 
	 * driver.findElement(objLoginPage.SourceName7).sendKeys(AssetSourceName);
	 * 
	 * Thread.sleep(1500); driver.findElement(objLoginPage.FaceAmount).clear();
	 * 
	 * Thread.sleep(1500);
	 * driver.findElement(objLoginPage.FaceAmount).sendKeys(FaceAmount);
	 * 
	 * driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();
	 * Thread.sleep(1500);
	 * 
	 * driver.findElement(By.xpath("//span[contains(.,'Next')]")).click();
	 * 
	 * Thread.sleep(1500);
	 * 
	 * }
	 */

	public static void lifeInsurance(String AssetSourceName, String FaceAmount, String isMultipleRows)
			throws Exception {
		// TODO Auto-generated method stub

		JavascriptExecutor je = (JavascriptExecutor) driver;

		// Identify the WebElement which will appear after scrolling down

		WebElement add = driver.findElement(By.xpath("//span[contains(.,'Add')]"));

		// now execute query which actually will scroll until that element is not
		// appeared on page.

		je.executeScript("arguments[0].scrollIntoView(true);", add);

		driver.findElement(By.xpath("//span[contains(.,'Add')]")).click();
		Thread.sleep(1500);

		driver.findElement(objLoginPage.SourceName7).sendKeys(AssetSourceName);

		Thread.sleep(1500);
		driver.findElement(objLoginPage.FaceAmount).clear();

		Thread.sleep(1500);
		driver.findElement(objLoginPage.FaceAmount).sendKeys(FaceAmount);

		driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();
		Thread.sleep(1500);

		addnextAssetData(isMultipleRows);

	}

	public static int j = 1;

	private static void addnextLiabllityData(String isMultipleRows)
			throws Exception, InvalidFormatException, IOException, InterruptedException, Exception {
		// TODO Auto-generated method stub
		if (isMultipleRows.equalsIgnoreCase("N")) {
			driver.findElement(By.xpath("//span[contains(.,'Next')]")).click();
			Thread.sleep(1500);
		} else if (isMultipleRows.equalsIgnoreCase("Y")) {
			new Liabilities().nextLiabilityData(j++);
			/*
			 * while (nextIncome) { nextIncome = new Liabilities().nextLiabilityData(i); if
			 * (!nextIncome) { break; } }
			 * driver.findElement(By.xpath("//span[contains(.,'Next')]")).click();
			 * Thread.sleep(1500);
			 */
		}

	}

	public static void BankCreditUnion(String BankSourceName, String NameofBank, String Accountnumber,
			String CashorMarketValue, String type, String isMultipleRows) throws Exception {

		JavascriptExecutor je = (JavascriptExecutor) driver;

		// Identify the WebElement which will appear after scrolling down

		WebElement add = driver.findElement(By.xpath("//span[contains(.,'Add')]"));

		// now execute query which actually will scroll until that element is not
		// appeared on page.

		je.executeScript("arguments[0].scrollIntoView(true);", add);

		driver.findElement(By.xpath("//span[contains(.,'Add')]")).click();
		Thread.sleep(1500);

		driver.findElement(objLoginPage.SourceName7).sendKeys(BankSourceName);

		Thread.sleep(1500L);

		driver.findElement(objLoginPage.NameofBank).sendKeys(NameofBank);

		Thread.sleep(1500L);

		driver.findElement(objLoginPage.Accountnumber).sendKeys(Accountnumber);

		Thread.sleep(1500L);

		driver.findElement(objLoginPage.CashorMarketValue).sendKeys(CashorMarketValue);

		Thread.sleep(1500L);
		WebElement element1127 = driver.findElement(objLoginPage.type1);
		Select oSelect27 = new Select(element1127);

		oSelect27.selectByValue(type);
		Thread.sleep(1500L);

		driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();

		Thread.sleep(1500L);

		addnextAssetData(isMultipleRows);

	}

	public static void StockBonds(String StockSourceName, String CashorMarketValue, String Stocktype,
			String isMultipleRows) throws Exception {

		JavascriptExecutor je = (JavascriptExecutor) driver;

		// Identify the WebElement which will appear after scrolling down

		WebElement add = driver.findElement(By.xpath("//span[contains(.,'Add')]"));

		// now execute query which actually will scroll until that element is not
		// appeared on page.

		je.executeScript("arguments[0].scrollIntoView(true);", add);

		driver.findElement(By.xpath("//span[contains(.,'Add')]")).click();
		Thread.sleep(1500);

		driver.findElement(objLoginPage.StkSourceName).sendKeys(StockSourceName);

		Thread.sleep(1500L);

		driver.findElement(objLoginPage.CashorMarketValue).sendKeys(CashorMarketValue);

		Thread.sleep(1500L);
		WebElement element1127 = driver.findElement(objLoginPage.type1);
		Select oSelect27 = new Select(element1127);

		oSelect27.selectByValue(Stocktype);
		Thread.sleep(1500L);

		driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();

		Thread.sleep(1500L);

		addnextAssetData(isMultipleRows);

	}

	public static void RetirementFund(String RetirementFundSourceName, String VestedInterestinRetirementFund,
			String isMultipleRows) throws Exception {
		JavascriptExecutor je = (JavascriptExecutor) driver;

		WebElement add = driver.findElement(By.xpath("//span[contains(.,'Add')]"));

		je.executeScript("arguments[0].scrollIntoView(true);", add);

		driver.findElement(By.xpath("//span[contains(.,'Add')]")).click();
		Thread.sleep(1500);

		driver.findElement(objLoginPage.RFSourceNAme).sendKeys(RetirementFundSourceName);

		Thread.sleep(1500L);

		driver.findElement(objLoginPage.VestedinRF).sendKeys(VestedInterestinRetirementFund);

		driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();

		Thread.sleep(1500L);

		addnextAssetData(isMultipleRows);

	}

	public static void Business(String BusinessSourceName, String NetWorthBusinessOwned, String isMultipleRows)
			throws Exception {
		JavascriptExecutor je = (JavascriptExecutor) driver;

		je.executeScript("scroll(0, 250);");

		driver.findElement(By.xpath("//span[contains(.,'Add')]")).click();
		Thread.sleep(1500);

		driver.findElement(objLoginPage.BoSourceNAme).sendKeys(BusinessSourceName);

		Thread.sleep(1500L);

		driver.findElement(objLoginPage.netwBo).sendKeys(NetWorthBusinessOwned);

		driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();

		Thread.sleep(2500L);

		addnextAssetData(isMultipleRows);

	}

	public static void Liabilities(String Category) throws InterruptedException, AWTException {
		// TODO Auto-generated method stub
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_PAGE_DOWN);
		WebElement lic = driver.findElement(objLoginPage.Licat);
		Select oSelect27 = new Select(lic);

		oSelect27.selectByValue(Category);

		Thread.sleep(3500);

	}

	public static void Alimony(String Type, String PaymentsOwedto, String MonthlyAmount, String isMultipleRows)
			throws InvalidFormatException, IOException, Exception {

		WebElement type1 = driver.findElement(objLoginPage.libtype);
		Select oSelect27 = new Select(type1);

		oSelect27.selectByValue(Type);

		Thread.sleep(3500);

		driver.findElement(By.xpath("//span[contains(.,'Add')]")).click();
		Thread.sleep(1500);

		driver.findElement(objLoginPage.paymentowed).sendKeys(PaymentsOwedto);
		Thread.sleep(2500);
		driver.findElement(objLoginPage.MonthlyAount).sendKeys(MonthlyAmount);

		driver.findElement(By.xpath(
				"//input[@type='checkbox' and following-sibling::label/span = 'Liability will be paid by closing']"))
				.click();

		Thread.sleep(2500);

		driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();
		Thread.sleep(3500);

		addnextLiabllityData(isMultipleRows);
	}

	public static void JobExpense(String Type, String expenses, String isMultipleRows)
			throws InvalidFormatException, IOException, Exception {

		WebElement type1 = driver.findElement(objLoginPage.libtype);
		Select oSelect27 = new Select(type1);

		oSelect27.selectByValue(Type);

		driver.findElement(By.xpath("//span[contains(.,'Add')]")).click();
		Thread.sleep(1500);
		driver.findElement(objLoginPage.jexp).sendKeys(expenses);
		Thread.sleep(1500);

		driver.findElement(By.xpath(
				"//input[@type='checkbox' and following-sibling::label/span = 'Liability will be paid by closing']"))
				.click();

		Thread.sleep(1500);

		driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();
		Thread.sleep(3500);
		addnextLiabllityData(isMultipleRows);

	}

	public static void Declaration(String judgements, String lawsuit, String LoanGurantee, String Cashtoclose,
			String Areyouacomakerorendorseronanote, String USCitizen, String PermanentResidentAlien,
			String occupythepropertyasyourprimaryresidence, String filedforanybankruptcythathasnotyetbeenresolved,
			String AdditionalRemarks, String Ethnicity, String Race, String EnrollmentTribe, String Sex)
			throws InterruptedException, AWTException {

		WebElement st_1 = driver.findElement(By.id("//input[@id='input-13-radio-0']"));
		((JavascriptExecutor)driver).executeScript("arguments[0].click();", st_1 );
		// next2 =	driver.findElement(By.xpath("//input[@id='input-1-radio-0'][@value='Yes']")).click();
		Thread.sleep(1500);
		//driver.findElement(By.xpath("//input[@id='input-2-radio-0'][@value='Yes']")).click();
		
		
		
	}
		
	/*
		WebElement element1123 = driver.findElement(objLoginPage.judgements);
		Select oSelect23 = new Select(element1123);

		oSelect23.selectByValue(judgements);
		Thread.sleep(3000);

		WebElement element1124 = driver.findElement(objLoginPage.lawsuit);
		Select oSelect24 = new Select(element1124);

		oSelect24.selectByValue(lawsuit);
		Thread.sleep(3000);
		*/

		// JavascriptExecutor je1 = (JavascriptExecutor) driver;

		// Identify the WebElement which will appear after scrolling down

		// WebElement next2 = driver.findElement(objLoginPage.LoanGurantee);

		// now execute query which actually will scroll until that element is not
		// appeared on page.

		// je1.executeScript("arguments[0].scrollIntoView(true);",next2);

		/*
		WebElement element1125 = driver.findElement(objLoginPage.LoanGurantee);
		Select oSelect25 = new Select(element1125);

		oSelect25.selectByValue(LoanGurantee);
		Thread.sleep(1500);

		WebElement element1126 = driver.findElement(objLoginPage.Cashtoclose);
		Select oSelect26 = new Select(element1126);

		oSelect26.selectByValue(Cashtoclose);
		Thread.sleep(1500);
		WebElement element1127 = driver.findElement(objLoginPage.endorser);
		Select oSelect27 = new Select(element1127);

		oSelect27.selectByValue(Areyouacomakerorendorseronanote);
		Thread.sleep(1500);

		WebElement element1128 = driver.findElement(objLoginPage.UScitizen);
		Select oSelect28 = new Select(element1128);

		oSelect28.selectByValue(USCitizen);
		Thread.sleep(1500);
		WebElement element1129 = driver.findElement(objLoginPage.permanent);
		Select oSelect29 = new Select(element1129);

		oSelect29.selectByValue(PermanentResidentAlien);
		Thread.sleep(1500);
		WebElement element1130 = driver.findElement(objLoginPage.occupy);
		Select oSelect30 = new Select(element1130);

		oSelect30.selectByValue(occupythepropertyasyourprimaryresidence);

		Thread.sleep(1500);
		WebElement element1131 = driver.findElement(objLoginPage.bankruptcy);
		Select oSelect31 = new Select(element1131);

		oSelect31.selectByValue(filedforanybankruptcythathasnotyetbeenresolved);
		Thread.sleep(1500L);

		driver.findElement(objLoginPage.ADR).sendKeys(AdditionalRemarks);
		Thread.sleep(1500);
		WebElement element1132 = driver.findElement(objLoginPage.ethinicity);
		Select oSelect32 = new Select(element1132);
		oSelect32.selectByValue(Ethnicity);
		Thread.sleep(1500);
		WebElement element1133 = driver.findElement(objLoginPage.race);
		Select oSelect33 = new Select(element1133);
		oSelect33.selectByValue(Race);

		Thread.sleep(1500);

		// JavascriptExecutor je1 = (JavascriptExecutor) driver;
		((JavascriptExecutor) driver).executeScript("scroll(0,400)");

		// WebElement next2 =
		// driver.findElement(By.xpath("//span[contains(.,'Save')]"));
		// je1.executeScript("arguments[0].scrollIntoView(true);",next2);

		// driver.findElement(By.xpath("//*[@id=\"6174:0\"]")).sendKeys(EnrollmentTribe);

		driver.findElement(objLoginPage.Enrolltribe).sendKeys(EnrollmentTribe);

		Thread.sleep(1500);
		WebElement element1134 = driver.findElement(objLoginPage.sex);
		Select oSelect34 = new Select(element1134);
		oSelect34.selectByValue(Sex);
		Thread.sleep(3500);
		driver.findElement(By.xpath("//*[contains(text(), 'Save')]")).click();

		Thread.sleep(2500L);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_PAGE_DOWN);

		Thread.sleep(1500);

		driver.findElement(By.xpath("//*[contains(text(), 'Next')]")).click();
		Thread.sleep(2500L);

		Thread.sleep(1500);
	}
	*/

	public static void LoanDetails(String ClosingAgentEscrowOfficer, String TitleCompany, String ContactName,
			String Contactph, String HastheClientpaidFee, String Story) throws InterruptedException {

		WebElement element1135 = driver.findElement(objLoginPage.escrowofficer);
		Select oSelect35 = new Select(element1135);
		oSelect35.selectByValue(ClosingAgentEscrowOfficer);
		Thread.sleep(3500);

		driver.findElement(objLoginPage.ttlcmpny).sendKeys(TitleCompany);
		Thread.sleep(2500);

		driver.findElement(objLoginPage.ContactName).sendKeys(ContactName);

		Thread.sleep(3500);

		driver.findElement(objLoginPage.Contactph).sendKeys(Contactph);

		Thread.sleep(2500);

		WebElement element1136 = driver.findElement(objLoginPage.clientfee);
		Select oSelect36 = new Select(element1136);
		oSelect36.selectByValue(HastheClientpaidFee);
		Thread.sleep(1500);

		driver.findElement(objLoginPage.Story).sendKeys(Story);
		Thread.sleep(1000);

		driver.findElement(By.xpath("//*[contains(text(), 'Save and Continue')]")).click();

	}

	public static void LoanContacts(String LoanOfficer, String LoanProcessor, String PreferredContact)
			throws InterruptedException {
		// TODO Auto-generated method stub

		WebElement element1137 = driver.findElement(objLoginPage.loanoff);
		Select oSelect37 = new Select(element1137);
		oSelect37.selectByValue(LoanOfficer);
		Thread.sleep(3500);
		WebElement element1138 = driver.findElement(objLoginPage.loanProc);
		Select oSelect38 = new Select(element1138);
		oSelect38.selectByValue(LoanProcessor);
		Thread.sleep(3500);
		WebElement element1139 = driver.findElement(objLoginPage.prefcont);
		Select oSelect39 = new Select(element1139);
		oSelect39.selectByValue(PreferredContact);
		Thread.sleep(3500);
		driver.findElement(By.xpath("//*[contains(text(), 'Save and Continue')]")).click();

	}

	public static void CreditInfo(String Cindicator) throws Exception {

		WebElement element1138 = driver.findElement(objLoginPage.Cindicator);
		Select oSelect38 = new Select(element1138);
		oSelect38.selectByValue(Cindicator);
		Thread.sleep(3500);
		driver.findElement(By.xpath("//*[contains(text(), 'Save')]")).click();
		Thread.sleep(3500);
		driver.findElement(objLoginPage.Rapp).click();
		Thread.sleep(3500);
		driver.quit();
	}

	public static void OthersFormsOfIncome(String OtherformsofIncome, String OtherformsofSource, String Otherincome)
			throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(3000);

		WebElement element1122 = driver.findElement(objLoginPage.OtherformsofIncome);
		Select oSelect22 = new Select(element1122);

		oSelect22.selectByValue(OtherformsofIncome);
		Thread.sleep(3000);

		JavascriptExecutor je = (JavascriptExecutor) driver;

		// Identify the WebElement which will appear after scrolling down

		WebElement next = driver.findElement(By.xpath("//span[contains(.,'Add Income')]"));

		// now execute query which actually will scroll until that element is not
		// appeared on page.

		je.executeScript("arguments[0].scrollIntoView(true);", next);

		driver.findElement(By.xpath("//span[contains(.,'Add Income')]")).click();

		// driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[6]/table[1]/tbody/tr[5]/td[6]/a")).click();

		driver.findElement(objLoginPage.SourceName6).sendKeys(OtherformsofSource);
		Thread.sleep(1500L);

		driver.findElement(objLoginPage.Incomevalue6).clear();
		Thread.sleep(5000);
		driver.findElement(objLoginPage.Incomevalue6).sendKeys(Otherincome);
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();
		Thread.sleep(5000);
	}

	public static int i = 1;

	public static void addnextAssetData(String isMultipleRows) throws Exception {

		if (isMultipleRows.equalsIgnoreCase("N")) {
			driver.findElement(By.xpath("//span[contains(.,'Next')]")).click();
			Thread.sleep(1500);
		} else if (isMultipleRows.equalsIgnoreCase("Y")) {
			new Assets().nextAssetData(i++);
			/*
			 * boolean nextIncome = new Assets().nextAssetData(i); while (nextIncome) {
			 * nextIncome = new Assets().nextAssetData(i); if(!nextIncome) { break; } i++; }
			 * driver.findElement(By.xpath("//span[contains(.,'Next')]")).click();
			 * Thread.sleep(1500);
			 */
		}
	}
	

	public static void editMethod1(String SourceName, String Incomevalue) throws InterruptedException {
		Thread.sleep(3000L);
		driver.findElement(objLoginPage.SourceName).sendKeys(SourceName);
		Thread.sleep(1500L);

		driver.findElement(objLoginPage.Incomevalue).clear();
		Thread.sleep(5000);
		driver.findElement(objLoginPage.Incomevalue).sendKeys(Incomevalue);
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();
		Thread.sleep(5000);
	}

	public static void editMethod2(String SocialSecuritySourceName, String SocialSecurityIncomevalue)
			throws InterruptedException {
		driver.findElement(objLoginPage.SourceName2).sendKeys(SocialSecuritySourceName);
		Thread.sleep(5000);

		driver.findElement(objLoginPage.Incomevalue2).clear();
		Thread.sleep(2500L);
		driver.findElement(objLoginPage.Incomevalue2).sendKeys(SocialSecurityIncomevalue);
		Thread.sleep(2500L);
		driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();
		Thread.sleep(5000);

	}

	public static void editMethod3(String DividendSourceName, String DividendIncomevalue) throws InterruptedException {
		Thread.sleep(3500L);
		driver.findElement(objLoginPage.SourceName3).sendKeys(DividendSourceName);
		Thread.sleep(1500L);

		driver.findElement(objLoginPage.Incomevalue3).clear();
		Thread.sleep(2500L);
		driver.findElement(objLoginPage.Incomevalue3).sendKeys(DividendIncomevalue);
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();
		Thread.sleep(5000);
	}

	public static void editMethod4(String RentalSource, String RentalIncomevalue) throws InterruptedException {
		driver.findElement(objLoginPage.SourceName5).sendKeys(RentalSource);
		Thread.sleep(1500L);

		driver.findElement(objLoginPage.Incomevalue5).clear();
		Thread.sleep(5000);
		driver.findElement(objLoginPage.Incomevalue5).sendKeys(RentalIncomevalue);
		Thread.sleep(5000);
		driver.findElement(By.xpath("//span[contains(.,'Save')]")).click();
		Thread.sleep(3000);
	}
}
