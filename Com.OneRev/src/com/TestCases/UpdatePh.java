package com.TestCases;

import java.util.ArrayList;

import org.openqa.selenium.WebDriver;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Specific_Methods.TestCase_Specific;
import com.Specific_Methods.TestCase_Specific_Methods;
import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.CommonUtils;
import com.UtilityFiles.Constants;
import com.UtilityFiles.FPath;

public class UpdatePh {
	FPath FP;
	CommonFunctions common;
	CommonUtils com;
	TestCase_Specific_Methods spm;
	TestCase_Specific sp;
	WebDriver dr;
	Constants con;
	CommonUtils com1 = new CommonUtils();
	public ArrayList<Boolean> statusList1=new ArrayList<Boolean>();
	
	
	
	public boolean UpdatePh() throws InterruptedException{
		preCond();
		UpdatePhone();
		postCond();
		//statusList1.addAll(spm.statusList);
		statusList1.addAll(sp.statusList);
		return statusList1.contains(false);
		
	}
	
	
	 public boolean preCond() throws InterruptedException
	  {
		try {
			com1.getDriverObject();
			FP=new FPath();
			common=new CommonFunctions();
			//sp=new TestCase_Specific_Methods();
			sp=new TestCase_Specific();
			//dr=com.getDriverObject();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return false;
	  }
	
	public boolean UpdatePhone()
	{
		boolean tc_res = false;
		try {
			sp.SelectProfile(com1.drv);
					
			sp.LoginBriovaRx(com1.drv, Constants.ID, Constants.PWD);
			
			sp.UpdateMyProfileLink(com1.drv);
			
			sp.PhoneLink(com1.drv);
			sp.PhoneNumTxtFld(com1.drv);
			sp.SubmitButton(com1.drv);
			
			sp.SccMsgVerify(com1.drv);
			
			sp.LogoutBriovaRx(com1.drv);
			
		} catch (Exception e) {
			Reporter.log("Error " + e.getMessage()); 	
			tc_res = false;
		}
		return tc_res;
	}
	
	  public boolean postCond()
	  {
		  try {
			com1.drv.close();
			  com1.drv.quit();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		  return false;
	  }
}
