package com.OneReverse;

import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.FPath;
import com.UtilityFiles.ReportStatus;

public class Subjectproperty {
	static com.UtilityFiles.CommonFunctions data;
	public static final String FILEPATH_TESTDATA = FPath.TESTDATA;
	public static final String ID = "com.OneReverse.Subjectproperty";

	ReportStatus rs = new ReportStatus();

	public ReportStatus Subjectproperty(Integer rowCount) throws Exception {

		rs.setReportStatus(false);

		// public boolean Subjectproperty(Integer rowCount) throws Exception {
		// Load the selected Browser
		// PrimeClerk.loadUrl("Chrome");

		// Enter the login credentials from xl sheet.
		// PrimeClerk.login(data.getTestData(FILEPATH_TESTDATA, "UserName", ID),
		// data.getTestData(FILEPATH_TESTDATA, "Password", ID));

		OneRev.Subjectproperty(CommonFunctions.getTestData(FILEPATH_TESTDATA, "SubjectPropertyAddress", ID, rowCount),
				CommonFunctions.getTestData(FILEPATH_TESTDATA, "SubjectPropertycity", ID, rowCount),
				CommonFunctions.getTestData(FILEPATH_TESTDATA, "SubjectPropertyZipCode", ID, rowCount),
				CommonFunctions.getTestData(FILEPATH_TESTDATA, "LegalDescriptionofProperty", ID, rowCount),
				CommonFunctions.getTestData(FILEPATH_TESTDATA, "ResidenceType", ID, rowCount),
				CommonFunctions.getTestData(FILEPATH_TESTDATA, "noofunits", ID, rowCount),
				CommonFunctions.getTestData(FILEPATH_TESTDATA, "yearbuilt", ID, rowCount),
				CommonFunctions.getTestData(FILEPATH_TESTDATA, "propertyheldas", ID, rowCount),
				CommonFunctions.getTestData(FILEPATH_TESTDATA, "PropertyTitleisHeldinTheseNames", ID, rowCount),
				// CommonFunctions.getTestData(FILEPATH_TESTDATA,
				// "CheckIfTitleisalsoHeldAs",ID),
				CommonFunctions.getTestData(FILEPATH_TESTDATA, "RealEstateTaxesMonthly", ID, rowCount),
				CommonFunctions.getTestData(FILEPATH_TESTDATA, "HazardInsuranceMonthly", ID, rowCount),
				CommonFunctions.getTestData(FILEPATH_TESTDATA, "misc", ID, rowCount), null, null);

		return rs;

	}
}