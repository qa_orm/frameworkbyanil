package com.OneReverse;

/*
package com.primeclerk;

import com.UtilityFiles.FPath;

import com.primeclerk.*;

import java.io.FileInputStream;
import java.util.Date;
import java.util.List;
import com.primeclerk.objectProp.objLoginPage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.DriverScript.MasterScript;
import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.FPath;
import com.UtilityFiles.GenericLibrary;


public class Subject_property {

	
	
	//static POIFSFileSystem data;
//	public static final String FILEPATH_TESTDATA = FPath.TESTDATA;
	FileInputStream fileInputStream = new FileInputStream(FPath.CONFIGURATION);
	Workbook wb = WorkbookFactory.create(fileInputStream); {
	for (int x=0;x<wb.getNumberOfSheets();x++) {
		
		Sheet sheet =  wb.getSheetAt(x);
		
		String sh = wb.getSheetName(x);

	System.out.println(sh);
	}
	}
	
//	public static final String ID1 = "com.primeclerk.MasterScript";
//	public static final String ID = "com.primeclerk.Subject_property"
	
//	public static boolean Subject_property() throws Exception {
	// TODO Auto-generated method stub
	
		/*
		try{
		FileInputStream fileInputStream = new FileInputStream(FPath.CONFIGURATION);
		Workbook wb = WorkbookFactory.create(fileInputStream);

		for (int x=0;x<wb.getNumberOfSheets();x++) {
			
			Sheet sheet =  wb.getSheetAt(x);

			Sheet s = wb.getSheet("ORM");
			int rowCnt = s.getLastRowNum();
			GlobalScript=FPath.LOGS+MasterScript.ConstantTimeStamp+"/"+"mytest02"+".xlsx";
			GenericLibrary.Create_LogFile("mytest02");//point 1
			for(int j=1;j<=rowCnt;j++){
			       Counter=0;
			       org.apache.poi.ss.usermodel.Row row = s.getRow(j);
			       org.apache.poi.ss.usermodel.Cell cell = row.getCell(1);                                                              
			       String execStatus = cell.getRichStringCellValue().toString();
			       if(execStatus.equalsIgnoreCase("yes")){
			              org.apache.poi.ss.usermodel.Cell cell1 = row.getCell(0);
			             String scrptNm1=cell1.getRichStringCellValue().toString();
			             sheetName=scrptNm1;
			             String filePath=FPath.TESTDATA;
			             String testId[]=CommonFunctions.getTestCaseId(filePath);
			             String testId1[]=CommonFunctions.getTestCaseId(filePath,1);
			             for(String id:testId ){
			                   scriptName=id;
			                   System.out.println(scriptName+" "+scrptNm1);
			                   SourceSystem = testId1[Counter];
			                   MemId = testId1[Counter+1];

			                   int indexNo=scriptName.lastIndexOf(".")+1;
			                   String methodName=scriptName.substring(indexNo);
			                   GlobalScript=FPath.LOGS+MasterScript.ConstantTimeStamp+"/"+"mytest02"+".xlsx";
			                   Class<?> c;
			              String className= methodName;
			              long startTime = new Date().getTime();
			              c = Class.forName(scriptName); // ClassName
			              java.lang.reflect.Constructor<?> cons = c.getConstructor();
			              Object object = cons.newInstance();
			              java.lang.reflect.Method method= object.getClass().getMethod(className);
			              
			              //java.lang.reflect.Method method= c.getMethod(className);
			              Boolean bool =  (Boolean) method.invoke(object);
			              long stopTime = new Date().getTime(); // Stop Time for a individual test
			              //Casting of object to boolean
			              boolean b = Boolean.valueOf(bool.toString());
			              SuiteStop = new Date().getTime(); // End Time for entire suite
			              SuiteDuration = (SuiteStop-SuiteStart)/ 1000;
			              Duration= GenericLibrary.getTime(SuiteDuration);
			              Counter = Counter+2;
			              if (b==false) //To Check complete test case Pass/Fail
			              {
			                    GenericLibrary.setResultCell(className,"Pass"); 
			                    GenericLibrary.CreateHTMLReport(Duration, ConstantTimeStamp, ConstantTimeStamp);
			              }
			              else
			              {
			                    GenericLibrary.setResultCell(className, "Fail");
			                                  GenericLibrary.CreateHTMLReport(Duration, ConstantTimeStamp, ConstantTimeStamp);
			                           }
			                    }
			               
			                    }
			              }
			    }
			       catch (ClassNotFoundException e)
			                                                                         {
			              e.printStackTrace();
			              }      
			   }

			}
		
		
		
	// Load the selected Browser
	PrimeClerk.loadUrl("Chrome");

	// Enter the login credentials from xl sheet.
	PrimeClerk.login(data.getTestData(FILEPATH_TESTDATA, "UserName", ID),
			data.getTestData(FILEPATH_TESTDATA, "Password", ID));

	// Enter the appdate
	PrimeClerk.appdate(data.getTestData(FILEPATH_TESTDATA, "appdate", ID));

	Thread.sleep(3000L);

	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Subject Property Address *')]/../following-sibling::input")).sendKeys("test");
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Subject Property City *')]/../following-sibling::input")).sendKeys("Test city");
	
	Thread.sleep(500L);
	
	PrimeClerk.driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[3]/div/div/fieldset/div/div/div[5]/div/input")).sendKeys("23456");
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Legal Description of Property')]/../following-sibling::input")).sendKeys("23456");
	WebElement rtype=PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Residence Type *')]/../following-sibling::select"));
	
	Select sl11 = new Select(rtype);
	
	sl11.selectByIndex(1);
	
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'No of Units *')]/../following-sibling::input")).sendKeys("5");
	
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Year Built *')]/../following-sibling::input")).sendKeys("1990");
	
WebElement held=PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Property Held as *')]/../following-sibling::select"));
	
	Select sl12 = new Select(held);
	
	sl12.selectByIndex(1);
	
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Property Title is Held in These Names')]/../following-sibling::input")).sendKeys("Testing Selenium");
	
	
	
WebElement title=PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Check If Title is also Held As')]/../following-sibling::select"));
	
	Select sl13 = new Select(title);
	
	sl13.selectByIndex(1);
	
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Real Estate Taxes - Monthly *')]/../following-sibling::input")).sendKeys("1990");
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Hazard Insurance - Monthly *')]/../following-sibling::input")).sendKeys("1990");
	PrimeClerk.driver.findElement(By.xpath("//span[contains(text(),'Miscellaneous (HOA, PUD, Condo Fees, Other Insurances) - Monthly')]/../following-sibling::input")).sendKeys("1990");
	PrimeClerk.driver.findElement(By.xpath(
			"//button[contains(.,'Save')]")).click();
	
	return false;
	
	
}
	
	
}

*/
