package com.primeclerk.objectProp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class objLoginPage {
	
	//public static String PrimeClerkMItem;
	
	

	//public static final By type = null;
	public static By txtUsername = By.id("loginPage:loginForm:username"); 
	public static By txtPassword = By.id("loginPage:loginForm:password"); 
	public static By btnSubmit   = By.id("loginPage:loginForm:loginButton"); 
	public static By sanl	=By.xpath("html/body/div[*]//forcecommunity-global-navigation-list/div/nav/ul/li[3]/a/span");
	
//	public static By sanl123		=By.xpath(".//*[@id='navigationMenu']/li[3]/div/a");
	public static By appdate1 	=By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div/div[3]/div/form/div[1]/div/input");
	public static By appdate 	=By.xpath("//div[@class='form-element uiInput uiInputDate uiInput--default uiInput--input uiInput--datetime']");
	
	public static By Mrtgappfor = By.xpath("//span[contains(text(),'Mortgage Applied for ')]/../following-sibling::select");
	
	public static By Pol = By.xpath("//span[contains(text(),'Purpose of Loan ')]/../following-sibling::select");
	
	public static By Slpplan	= By.xpath("//span[contains(text(),'Selected Loan Payment Plan ')]/../following-sibling::select");
	
	public static By LoanOriginationFeeCalculationMethod 		= By.xpath("//span[contains(text(),'Loan Origination Fee - Calculation Method ')]/../following-sibling::select");
	
	public static By Thisapplicationwastakenby = By.xpath("//span[contains(text(),'This application was taken by ')]/../following-sibling::select");
	
	public static By LoanOriginatorsFirstName = By.xpath("//span[contains(text(),'Loan Originator�s First Name ')]/../following-sibling::input");
	
	public static By LoanOriginatorsLastName = By.xpath("//span[contains(text(),'Loan Originator�s Last Name ')]/../following-sibling::input");
	
	public static By InterviewDate 	= By.xpath("//*[@class='slds-input Datecontrolwidth input']");
	
	public static By LoanOriginatorsPhoneNumber = By.xpath("//span[contains(text(),'Loan Originator�s Phone Number ')]/../following-sibling::input");
	
	public static By LoanOriginationCompanysName = By.xpath("//span[contains(text(),'Loan Origination Company�s Name ')]/../following-sibling::input");
	
	public static By LoanOriginationCompanysAddress = By.xpath("//span[contains(text(),'Loan Origination Company�s Address ')]/../following-sibling::input");
	
	public static By LoanOriginationCompanysCity = By.xpath("//span[contains(text(),' Loan Origination Company�s City ')]/../following-sibling::input");
	
	public static By LoanOriginationCompanysState = By.xpath("//span[contains(text(),'Loan Origination Company�s State ')]/../following-sibling::select");
			//span[contains(text(),'Loan Origination Company�s State ')]/../following-sibling::select");
	
	public static By LoanOriginationCompanysZipCode = By.xpath("//span[contains(text(),'Loan Origination Company�s Zip Code ')]/../following-sibling::input");
	
	public static By LoanOriginatorIdentifier = By.xpath("//span[contains(text(),'Loan Originator Identifier')]/../following-sibling::input");
	
	public static By household = By.xpath("//span[contains(text(),'Household Members ')]/../following-sibling::select");
	
	public static By Childrenlivingunderage = By.xpath("//span[contains(text(),'Children under the age of 6 living in the home? ')]/../following-sibling::select");
	
	public static By POA	= By.xpath("//span[contains(text(),'Is there a POA? ')]/../following-sibling::select");
	
	public static By NameofPOA	= By.xpath("//span[contains(text(),'If yes, Name of POA')]/../following-sibling::input");
	
	//public static By Map		= By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[1]/div/select");
	public static By EAV		= By.xpath("//span[contains(text(),'Estimate of Appraised Value ')]/../following-sibling::input");
	
	public static By sqfootge 	= By.xpath("//span[contains(text(),'Square Footage ')]/../following-sibling::input");
	
	public static By AltcontactName = By.xpath("//span[contains(text(),'Alternate Contact Name ')]/../following-sibling::input");
	
	public static By AltcontactNamefulladdress = By.xpath("//span[contains(text(),'Alternate Contact Full Address ')]/../following-sibling::input");
	
	public static By AltcontactNamephno = By.xpath("//span[contains(text(),'Alternate Contact Phone Number ')]/../following-sibling::input");
	
	public static By rlshpaltcont = By.xpath("//span[contains(text(),'Relationship to Alternative Contact')]/../following-sibling::select");
	
	public static By Margin = By.xpath("//span[contains(text(),'Margin')]/../following-sibling::select");
	
	//-------------------------------Subject property---------------------------
	
	public static By SubjectPropertyAddress = By.xpath("//span[contains(text(),'Subject Property Address *')]/../following-sibling::input");
	
	public static By SubjectPropertycity = By.xpath("//span[contains(text(),'Subject Property City *')]/../following-sibling::input");
	
	public static By SubjectPropertyZipCode = By.xpath("//span[contains(text(),'Subject Property Zip Code ')]/../following-sibling::input");
	
	public static By LegalDescriptionofProperty = By.xpath("//span[contains(text(),'Legal Description of Property')]/../following-sibling::input");
	
	public static By ResidenceType		= By.xpath("//span[contains(text(),'Residence Type *')]/../following-sibling::select");
	
	public static By noofunits = By.xpath("//span[contains(text(),'No of Units *')]/../following-sibling::input");
	
	public static By yearbuilt = By.xpath("//span[contains(text(),'Year Built *')]/../following-sibling::input");
	
	public static By propertyheldas = By.xpath("//span[contains(text(),'Property Held as *')]/../following-sibling::select");
	
	public static By PropertyTitleisHeldinTheseNames = By.xpath("//span[contains(text(),'Property Title is Held in These Names')]/../following-sibling::input");
	
	public static By CheckIfTitleisalsoHeldAs = By.xpath("//span[contains(text(),'Check If Title is also Held As')]/../following-sibling::select");
	
	public static By RealEstateTaxesMonthly = By.xpath("//span[contains(text(),'Real Estate Taxes - Monthly *')]/../following-sibling::input");

	public static By HazardInsuranceMonthly = By.xpath("//span[contains(text(),'Hazard Insurance - Monthly *')]/../following-sibling::input");

	public static By misc = By.xpath("//span[contains(text(),'Miscellaneous (HOA, PUD, Condo Fees, Other Insurances) - Monthly')]/../following-sibling::input");
	
	
	//----------------------------------------Client---------------------------------------------------------------------------
	
	
	public static By FirstName = By.xpath("//span[contains(text(),'First Name ')]/../following-sibling::input");
	
	public static By MiddleName = By.xpath("//span[contains(text(),'Middle Name')]/../following-sibling::input");
	
	public static By LastName = By.xpath("//span[contains(text(),'Last Name ')]/../following-sibling::input");
	
	public static By CurrentAddress = By.xpath("//span[contains(text(),'Current Address ')]/../following-sibling::input");
	
	public static By CurrentCity = By.xpath("//span[contains(text(),'Current City ')]/../following-sibling::input");
	
	public static By CurrentState = By.xpath("//span[contains(text(),'Current State')]/../following-sibling::select");
	
	public static By CurrentZip = By.xpath("//span[contains(text(),'Current Zip ')]/../following-sibling::input");

	public static By NoofYrsCurrentAddress = By.xpath("//span[contains(text(),'Number of Years in Current Address ')]/../following-sibling::input");
	
	public static By email = By.xpath("//span[contains(text(),'Client Email Address ')]/../following-sibling::input");
	
	public static By HomePhone = By.xpath("//span[contains(text(),'Home Phone ')]/../following-sibling::input");
	
	public static By MobilePhone = By.xpath("//span[contains(text(),'Mobile Phone')]/../following-sibling::input");
	
	public static By DateofBirth = By.xpath("//span[contains(text(),'Date of Birth')]/../following-sibling::div/input");
	
	public static By SSN = By.xpath("//span[contains(text(),'Social Security Number ')]/../following-sibling::input");
	
	public static By MaritalStatus = By.xpath("//span[contains(text(),'Marital Status')]/../following-sibling::select");
			//+ "//span[contains(text(),'Marital Status')]/../following-sibling::select");
	
	public static By NBS	= By.xpath("//span[contains(text(),'Non-Borrowing Spouse *')]/../following-sibling::select");
	
	public static By NBSName	 = By.xpath("//span[contains(text(),'Non-Borrowing Spouse Name ')]/../following-sibling::input");
	
	public static By NBSPhone = By.xpath("//span[contains(text(),'Non-Borrowing Spouse Phone Number ')]/../following-sibling::input");
	
	public static By NBSdob		= By.xpath("(//span[contains(text(),'Date of Birth')]/../following-sibling::div/input)[2]");
	
	
	public static By NBSRel	 = By.xpath("//span[contains(text(),'Non-Borrower Relationship ')]/../following-sibling::select");
	
	//public static By CurrentAddress = By.xpath("//span[contains(text(),'Current Address ')]/../following-sibling::input");
	
	
	//-----------------------------Employment--------------------------------------------------
	
	public static By selfemployed = By.xpath("//span[contains(text(),'Self Employed ')]/../following-sibling::select");
	
	public static By IncomeType = By.xpath("//span[contains(text(),'IncomeType ')]/../following-sibling::select");
	
	public static By EmployerName = By.xpath("//span[contains(text(),'Employer Name')]/../following-sibling::input");
	
	public static By PosTitle = By.xpath("//span[contains(text(),'Position/Title')]/../following-sibling::input");
	
	public static By EmployerPhone = By.xpath("//span[contains(text(),'Employer Phone')]/../following-sibling::input");
	
	public static By EmployerAddress = By.xpath("//span[contains(text(),'Employer Address')]/../following-sibling::input");
	
	public static By City = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[5]/div/fieldset/div[2]/div/div[2]/div/fieldset/div/div/div[3]/div[5]/input");
	
	public static By State = By.xpath("//span[contains(text(),'State')]/../following-sibling::select");
	
	public static By Zip = By.xpath("//span[contains(text(),'Zip')]/../following-sibling::input");
	
	public static By Years = By.xpath("//span[contains(text(),'Years ')]/../following-sibling::input");
	
	public static By Months = By.xpath("//span[contains(text(),'Months')]/../following-sibling::input");
	
	public static By YearsEmployed = By.xpath("//span[contains(text(),'Years Employed in Profession (optional)')]/../following-sibling::select");
	
	//---------------------------------------------------------------------
	
	public static By SourceName = By.xpath("//span[contains(text(),'Source Name')]/../following-sibling::input");
	
	public static By Incomevalue = By.xpath("//span[contains(text(),'Income Value')]/../following-sibling::input");
	
	public static By SourceName2 = By.xpath("//span[contains(text(),'Source Name')]/../following-sibling::input");
	
	public static By Incomevalue2 = By.xpath("//span[contains(text(),'Income Value')]/../following-sibling::input");
	public static By SourceName3 = By.xpath("//span[contains(text(),'Source Name')]/../following-sibling::input");
	
	public static By Incomevalue3 = By.xpath("//span[contains(text(),'Income Value')]/../following-sibling::input");
	public static By SourceName4 = By.xpath("//span[contains(text(),'Source Name')]/../following-sibling::input");
	
	public static By Incomevalue4 = By.xpath("//span[contains(text(),'Income Value')]/../following-sibling::input");
	public static By SourceName5 = By.xpath("//span[contains(text(),'Source Name')]/../following-sibling::input");
	
	public static By Incomevalue5 = By.xpath("//span[contains(text(),'Income Value')]/../following-sibling::input");
	
	public static By OtherformsofIncome = By.xpath("//span[contains(text(),'Other forms of income')]/../following-sibling::select");
	
public static By SourceName6 = By.xpath("//span[contains(text(),'Source Name')]/../following-sibling::input");
	
	public static By Incomevalue6 = By.xpath("//span[contains(text(),'Income Value')]/../following-sibling::input");
	//------------------------------Assets-----------------------------------------------------------
	
	public static By Category = By.xpath("//span[contains(text(),'Category')]/../following-sibling::select");
	
	public static By SourceName7 = By.xpath("//span[contains(text(),'Source Name *')]/../following-sibling::input");
	
	public static By FaceAmount = By.xpath("//span[contains(text(),'Face Amount *')]/../following-sibling::input");
	//----------------------------------------------------------------------------------------------------------------
	public static By libCategory = By.xpath("//span[contains(text(),'Category')]/../following-sibling::select");
	
	public static By Type = By.xpath("//span[contains(text(),'Type')]/../following-sibling::select");
	
	public static By paymentowed = By.xpath("//span[contains(text(),'Payments Owed to')]/../following-sibling::input");
	
	public static By MonthlyAount = By.xpath("//span[contains(text(),'Monthly Amount')]/../following-sibling::input");
	
	public static By judgements = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[9]/fieldset/div[6]/div[2]/select");
	
	public static By lawsuit = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[9]/fieldset/div[6]/div[4]/select");
	
	public static By LoanGurantee	= By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[9]/fieldset/div[6]/div[6]/select");
	
	public static By Cashtoclose = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[9]/fieldset/div[6]/div[9]/select");
	
	//-----------------------------------------------------------assets-----------------------------------
	
	public static By StkSourceName = By.xpath("//span[contains(text(),'Source Name *')]/../following-sibling::input");
	
	
	public static By NameofBank = By.xpath("//span[contains(text(),'Source Name *')]/../following-sibling::input");
	
	public static By Accountnumber = By.xpath("//span[contains(text(),'Account Number (Optional)')]/../following-sibling::input");
	
	public static By CashorMarketValue = By.xpath("//span[contains(text(),'Cash or Market Value *')]/../following-sibling::input");
	
	public static By type1 = By.xpath("//span[contains(text(),'Type *')]/../following-sibling::select");
	
	public static  By libtype = By.xpath("//span[contains(text(),'Type')]/../following-sibling::select");
	
	public static By LISourceName = By.xpath("//span[contains(text(),'Source Name *')]/../following-sibling::input");
	
	public static By LIFaceAmount = By.xpath("//span[contains(text(),'Face Amount *')]/../following-sibling::input");
	
	public static By RFSourceNAme = By.xpath("//span[contains(text(),'Source Name *')]/../following-sibling::input");
	
	public static By VestedinRF = By.xpath("//span[contains(text(),'Vested Interest in Retirement Fund *')]/../following-sibling::input");
	
	public static By BoSourceNAme = By.xpath("//span[contains(text(),'Source Name *')]/../following-sibling::input");
	
	public static By netwBo = By.xpath("//span[contains(text(),'Net Worth of Business(es) Owned *')]/../following-sibling::input");
	//--------------------------------------------------------------------------------declaration--------------------------------------------------------------
	
	public static By endorser = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[9]/fieldset/div[6]/div[12]/select");
	
	public static By UScitizen = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[9]/fieldset/div[6]/div[14]/select");
	
	public static By permanent = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[9]/fieldset/div[6]/div[16]/select");
	
	public static By occupy  = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[9]/fieldset/div[6]/div[18]/select");
	
	public static By bankruptcy  = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[9]/fieldset/div[6]/div[20]/select");
	
	public static By ADR = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[9]/fieldset/div[6]/div[27]/div[2]/textarea");
	
	
	public static By ethinicity = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[9]/fieldset/div[6]/div[29]/select");
	
	public static By race = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[9]/fieldset/div[6]/div[32]/select");
	
	
	public static By sex = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[9]/fieldset/div[6]/div[36]/select");
	
	
	public static By Enrolltribe = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[9]/fieldset/div[6]/div[33]/div[2]/input");
	
	public static By escrowofficer = By.xpath("//span[contains(text(),'Will you be using Title Source as the Closing Agent/Escrow Officer and Title Company? ')]/../following-sibling::select");
	
	public static By clientfee = By.xpath("//span[contains(text(),'Has the client paid or will they pay any fees to the partner for any services, excluding third party services, prior to closing? If so, the Advanced Fee disclosure applies. ')]/../following-sibling::select");
	
	public static By Story = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[10]/div/div/fieldset/div/div/div[6]/div/textarea");
	//-----------------------------Loan Details-----------------------------------------------------------
	
	public static By loanoff = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[11]/fieldset/form/div[1]/div[1]/select");
			//+ "//span[contains(text(),'Loan Officer')]/../following-sibling::select");
	public static By loanProc = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[11]/fieldset/form/div[2]/div[1]/select");
			//+ "//span[contains(text(),'Loan Processor')]/../following-sibling::select");
	public static By prefcont = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[11]/fieldset/form/div[3]/div/select");
			//+ "//span[contains(text(),'Preferred Contact')]/../following-sibling::select");
	public static By Rapp = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[12]/fieldset/form/div[6]/button");
	public static By jexp = By.xpath("//span[contains(text(),'Monthly Job Related Expenses ')]/../following-sibling::input");
	
	public static By appl	=By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div/div[3]/div/h1");
	public static By rb01	=By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[3]/div/div[1]/input");
	public static By rb02	= By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[3]/div/div[2]/input");
	public static By Strtapp	= By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div/div[3]/div/div[4]/button");
	public static By mrtgapp	= By.xpath("(//div[contains(@class,'uiInputSelect')]/select)[1]");
	public static By Appraisedvalue = By.xpath("html/body/div[4]/div[2]/div/div[2]/div/div/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div/fieldset/div/div/div[4]/div/input");
	public static By pol = By.xpath("(//div[contains(@class,'uiInputSelect')]/select)[2]");
	
	public static By Licat = By.xpath("//span[contains(text(),'Category')]/../following-sibling::select");
	
	
	//-------------------- loan details
	
	public static By ttlcmpny = By.xpath("//span[contains(text(),'Title Company ')]/../following-sibling::input");
	
	public static By ContactName = By.xpath("//span[contains(text(),'Contact Name ')]/../following-sibling::input");
	
	public static By Contactph = By.xpath("//span[contains(text(),'Contact Phone Number ')]/../following-sibling::input");
	public static By Cindicator = By.xpath("//span[contains(text(),'Credit Indicator')]/../following-sibling::select");
	
//	public static By yearbuilt;
	//public static By FirstName;
	//public static By SSN;
//	public static By MaritalStatus;
//	public static By NBS;
//	public static By NBSdob;
	//public static By Zip;
	//public static By NameofBank;
	//public static By CashorMarketValue;
	//public static By permanent;
//	public static By escrowofficer;

	
	
	
	//public static By menuPrimeClerk = By.xpath("//*[@class='left_content']//a[text()='"+PrimeClerkMItem+"']");//Manage MML
	

	
	
	
	
	

	
		
		
	}
		
	
	
	
	

