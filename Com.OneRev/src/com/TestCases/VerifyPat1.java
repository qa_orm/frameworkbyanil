package com.TestCases;

import java.util.ArrayList;

import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.Specific_Methods.TestCase_Specific;
import com.Specific_Methods.TestCase_Specific_Methods;
import com.UtilityFiles.CommonFunctions;
import com.UtilityFiles.CommonUtils;
import com.UtilityFiles.Constants;
import com.UtilityFiles.FPath;
//import com.relevantcodes.extentreports.ExtentReports;
//import com.relevantcodes.extentreports.ExtentTest;

public class VerifyPat1 {
	//ExtentReports report;
	//ExtentTest loger;
	FPath FP;
	CommonFunctions common;
	CommonUtils com;
	TestCase_Specific_Methods spm;
	TestCase_Specific sp;
	
	Constants con;
    CommonUtils com1 = new CommonUtils();
    public ArrayList<Boolean> statusList1=new ArrayList<Boolean>();
    
	
		@BeforeMethod
	  public  boolean preCond() throws InterruptedException 
	  {
		
		try {
			FP=new FPath();
			common=new CommonFunctions();
			spm=new TestCase_Specific_Methods();
			sp=new TestCase_Specific();
			//dr=com.getDriverObject();
			com1.getDriverObject();
		} catch (Exception e) {
			
			System.out.println(e.getCause());
		}
		return false;
	  }
	
	@Test
	public boolean VerifyPatientInformation()
	{
		boolean tc_res = false;
		
		try {
			
		    sp.SelectProfile(com1.drv);
			sp.LoginBriovaRx(com1.drv, Constants.ID, Constants.PWD);
			Thread.sleep(5000);
			sp.Patient_Information_Name(com1.drv);
			sp.Patient_DOB(com1.drv);
			sp.Patient_EmailAddress(com1.drv);
			sp.Patient_ID(com1.drv);
			sp.Gender(com1.drv);
			sp.Address(com1.drv);
			sp.LogoutBriovaRx(com1.drv);
			statusList1.addAll(sp.statusList);
			statusList1.addAll(sp.statusList);
			return statusList1.contains(false);
			
		} catch (Exception e)
		{
			Reporter.log("Error " + e.getMessage()); 	
			tc_res = false;
		}
		return tc_res;
	}
	
	@AfterMethod
	public  boolean postCond()
	  {
		
		  com1.drv.close();
		  com1.drv.quit();
		return false;
		  
	  }
	  
	  

}

